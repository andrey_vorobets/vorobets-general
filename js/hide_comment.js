// place this to the field description
<script type="text/javascript">
jQuery(document).ready( function($) {
    if (typeof JIRA !== "undefined") {
        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e,context) {
            callCommentHideFunction();
        });
        callCommentHideFunction();
    }

    function callCommentHideFunction(){
        if(	($('#issue-workflow-transition-submit').val() =='Request Closure') ||
            ($('#issue-workflow-transition-submit').val() =='Close Issue') ||
            ($('#issue-workflow-transition-submit').val() =='Reject Closure')
        ) {
            // $(".wiki-edit").hide();
            $(".comment-input").hide();
            $("label[for='comment']").hide();
        }
    }
});
</script>


