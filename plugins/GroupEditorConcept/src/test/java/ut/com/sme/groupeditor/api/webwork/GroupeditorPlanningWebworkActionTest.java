package ut.com.sme.groupeditor.api.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.sme.groupeditor.api.webwork.GroupeditorPlanningWebworkAction;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class GroupeditorPlanningWebworkActionTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //GroupeditorPlanningWebworkAction testClass = new GroupeditorPlanningWebworkAction();

        throw new Exception("GroupeditorPlanningWebworkAction has no tests!");

    }

}
