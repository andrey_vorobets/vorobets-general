package ut.com.sme.groupeditor;

import org.junit.Test;
import com.sme.groupeditor.api.MyPluginComponent;
import com.sme.groupeditor.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}