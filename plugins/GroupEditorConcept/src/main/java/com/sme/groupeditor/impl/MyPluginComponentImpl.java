package com.sme.groupeditor.impl;

import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.bc.group.search.GroupPickerSearchService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.sme.groupeditor.api.MyPluginComponent;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.activeobjects.external.ActiveObjects;
//import com.sme.groupeditor.rest.ProjectSearchResource;

import javax.inject.Inject;
import javax.inject.Named;

@ExportAsService ({MyPluginComponent.class})
@Named ("myPluginComponent")
public class MyPluginComponentImpl implements MyPluginComponent
{
        @ComponentImport
        private ActiveObjects activeObjects;

        @ComponentImport
        private PageBuilderService pageBuilderService;

        @ComponentImport
        private final ApplicationProperties applicationProperties;

        @ComponentImport
        private GroupPickerSearchService groupPickerSearchService;

        @ComponentImport
        private ProjectManager projectManager;

        @ComponentImport
        private UserSearchService userSearchService;

        //@ComponentImport
        //private ProjectSearchResource

        @Inject
        public MyPluginComponentImpl(final ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    public String getName()
    {
        if(null != applicationProperties)
        {
            return "myComponent:" + applicationProperties.getDisplayName();
        }
        
        return "myComponent";
    }
}