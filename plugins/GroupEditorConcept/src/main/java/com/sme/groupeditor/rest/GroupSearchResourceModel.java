package com.sme.groupeditor.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "groups")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupSearchResourceModel {

    @XmlElement
    private String text;

    @XmlElement
    private String id;

    public GroupSearchResourceModel() {

    }

    public GroupSearchResourceModel(String text, String id) {
        this.text = text;
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
