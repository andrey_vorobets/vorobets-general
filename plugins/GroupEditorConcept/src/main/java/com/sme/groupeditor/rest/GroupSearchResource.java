package com.sme.groupeditor.rest;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.group.search.GroupPickerSearchService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Named
@Path("/group")
public class GroupSearchResource {

    private GroupPickerSearchService groupPickerSearchService;

    @Inject
    public void GroupPickerSearchService(GroupPickerSearchService groupPickerSearchService) {
        this.groupPickerSearchService = groupPickerSearchService;
    }

    // This endpoint will be reachable via http://localhost:2990/jira/rest/usersearchresource/1.0/group/health
    @GET
    @Path("/health")
    @Produces({MediaType.APPLICATION_JSON})
    @AnonymousAllowed
    public Response health() {
        return Response.ok("ok").build();
    }

    // http://localhost:2990/jira/rest/usersearchresource/1.0/group/search?query=TEST
    @GET
    @Path("/search")
    //@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON})
    //public Response getMessage()
    public Response searchGroups(@QueryParam("query") final String userQuery,
                                   @Context HttpServletRequest request ) {
        List<GroupSearchResourceModel> projects = findGroups(userQuery);
        return Response.ok(projects).build();
    }

    private List<GroupSearchResourceModel> findGroups(String query) {
        List<GroupSearchResourceModel> groupSearchResourceModels = new ArrayList<>();
        List<com.atlassian.crowd.embedded.api.Group> embeddedGroups = groupPickerSearchService.findGroups(query);
        if (embeddedGroups != null) {
            for (com.atlassian.crowd.embedded.api.Group group : embeddedGroups) {
                if(!group.getName().contains("jira")) {
                    groupSearchResourceModels.add(new GroupSearchResourceModel(group.getName(), group.getName()));
                }
            }
        }

        return groupSearchResourceModels;
    }

}
