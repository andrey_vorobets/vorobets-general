package com.sme.groupeditor.rest;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Named
@Path("/project")
public class ProjectSearchResource {

    private ProjectManager projectManager;

    @Inject
    //public ProjectSearchResource(ProjectSearchResource projectSearchResource) {this.projectSearchResource = projectSearchResource;}
    public void ProjectManager(ProjectManager projectManager) {
        this.projectManager = projectManager;
    }

    public ProjectSearchResource() {

    }

    // This endpoint will be reachable via http://localhost:2990/jira/rest/usersearchresource/1.0/project/health
    @GET
    @Path("/health")
    @Produces({MediaType.APPLICATION_JSON})
    @AnonymousAllowed
    public Response health() {
        return Response.ok("ok").build();
    }

    @GET
    @Path("/search")
    //@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON})
    //public Response getMessage()
    public Response searchProjects(@QueryParam("query") final String userQuery,
                                @Context HttpServletRequest request ) {
        List<ProjectSearchResourceModel> projects = findProjects(userQuery);
        return Response.ok(projects).build();
    }

    private List<ProjectSearchResourceModel> findProjects(String query) {
        List<ProjectSearchResourceModel> projectSearchResourceModels =
                new ArrayList<>();
        List<Project> projects = projectManager.getProjects();
        if (projects!=null) {
            for (Project project : projects) {
                if (project.getName().contains(query) || project.getKey().contains(query)) {
                    projectSearchResourceModels.add(new ProjectSearchResourceModel(project.getKey(), project.getKey()));
                }
            }
        }

        return projectSearchResourceModels;
    }

}
