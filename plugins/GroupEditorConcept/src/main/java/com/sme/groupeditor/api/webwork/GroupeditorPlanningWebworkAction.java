package com.sme.groupeditor.api.webwork;

import com.atlassian.jira.security.request.RequestMethod;
import com.atlassian.jira.security.request.SupportedMethods;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import javax.inject.Inject;
import javax.inject.Named;

@SupportedMethods({RequestMethod.GET})
@Named
public class GroupeditorPlanningWebworkAction extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(GroupeditorPlanningWebworkAction.class);

    @Inject
    private PageBuilderService pageBuilderService;

    @Override
    @SupportedMethods({RequestMethod.GET})
    public String execute() throws Exception {
        pageBuilderService.assembler().resources().requireWebResource(
                "com.sme.groupeditor.SMEGroupEditor:SMEGroupEditor-resources"
        ).requireWebResource(
                "com.sme.groupeditor.SMEGroupEditor:SMEGroupEditor-resources"
        );

        //return super.execute(); //returns SUCCESS
        return "groupeditor-planning-success";
    }

    public void setPageBuilderService(PageBuilderService pageBuilderService) {
        this.pageBuilderService = pageBuilderService;
    }
}
