package com.sme.groupeditor.api;

public interface MyPluginComponent
{
    String getName();
}