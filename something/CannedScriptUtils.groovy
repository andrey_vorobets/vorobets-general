package com.onresolve.jira.groovy.canned.utils

import com.atlassian.jira.issue.resolution.Resolution
import com.atlassian.jira.ComponentManager

import com.atlassian.jira.workflow.JiraWorkflow
import com.atlassian.jira.issue.priority.Priority
import com.atlassian.jira.event.type.EventTypeManager
import com.atlassian.jira.event.type.EventType
import org.ofbiz.core.entity.GenericValue
import com.opensymphony.workflow.loader.ActionDescriptor

import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.issue.link.IssueLinkType
import com.atlassian.jira.config.ConstantsManager
import com.atlassian.jira.issue.issuetype.IssueType
import com.atlassian.jira.workflow.WorkflowManager
import org.apache.log4j.Category
import com.atlassian.jira.bc.filter.SearchRequestService
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.issue.search.SearchRequest
import com.atlassian.jira.issue.fields.FieldManager
import com.atlassian.jira.issue.fields.Field
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.customfields.impl.SelectCFType
import com.atlassian.jira.issue.customfields.impl.MultiSelectCFType
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType

/**
 * User: jechlin2
 * Date: 13-Dec-2010
 * Time: 18:52:50
 */
class CannedScriptUtils {

    public static Category log = Category.getInstance(CannedScriptUtils.class)
    static final String OUTWARD_FIELD_NAME = "outward";
    static final String INWARD_FIELD_NAME = "inward";

    public static Map getResolutionOptions(boolean withBlankFirstEntry) {
        Map<String,String> rt = new LinkedHashMap()
        if (withBlankFirstEntry) {
            rt.put("", "None")
        }

        ComponentManager.getInstance().getConstantsManager().getResolutionObjects().collect {Resolution r ->
            rt.put(r.getId(), r.getName())
        }
        rt
    }

    public static Map getProjectOptions(boolean withBlankFirstEntry) {
        Map<String,String> rt = new LinkedHashMap()
        if (withBlankFirstEntry) {
            rt.put("", "")
        }

        ComponentManager.getInstance().getProjectManager().getProjects().collect {GenericValue gv ->
            rt.put(gv.key, gv.name)
        }
        rt
    }

    public static Map getPriorityOptions(boolean withBlankFirstEntry) {
        Map<String,String> rt = new LinkedHashMap()
        if (withBlankFirstEntry) {
            rt.put("", "")
        }

        ComponentManager.getInstance().getConstantsManager().getPriorityObjects().collect {Priority p ->
            rt.put(p.getId(), p.getName())
        }
        rt
    }

    public static Map getEvents(boolean withBlankFirstEntry) {
        Map<String,String> rt = new LinkedHashMap()
        if (withBlankFirstEntry) {
            rt.put("", "")
        }

        EventTypeManager eventTypeManager = ComponentManager.getInstance().getEventTypeManager()
        eventTypeManager.getEventTypes().each {EventType eventType ->
            rt.put(eventType.id as String, eventType.name)
        }

        rt
    }

    public static Map getAllWorkflowActions(boolean withBlankFirstEntry) {

        Map<String,String> rt = new LinkedHashMap<String,String>()

        if (withBlankFirstEntry) {
            rt.put("", "")
        }
        Map actions = new HashMap()
        WorkflowManager workflowManager = ComponentManager.getInstance().getWorkflowManager()
        workflowManager.getWorkflows().each {JiraWorkflow workflow ->
            if (workflow.hasDraftWorkflow()) {
                workflow = workflowManager.getDraftWorkflow(workflow.name)
            }
            workflow.getAllActions().each {ActionDescriptor ad ->
                String v = "${ad.name} ($ad.id)"
                if (! rt.values().contains(v)) {
                    rt.put("$ad.id ${ad.name}", v)
                }
            }
        }

        rt.putAll(actions)
        rt.sort {it.value}
    }

    public static Map getAllIssueTypes(boolean withBlankFirstEntry, boolean includingSubtasks = false) {
        Map<String,String> rt = [:] as Map<String,String>
        if (withBlankFirstEntry) {
            rt.put("", "")
        }

        ComponentManager componentManager = ComponentManager.getInstance()
        ConstantsManager constantsManager = componentManager.getConstantsManager()
        constantsManager.getAllIssueTypeObjects().each {IssueType issueType ->
            if (includingSubtasks || ! issueType.isSubTask()) {
                rt.put(issueType.getId(), issueType.getName())
            }
        }

        rt
    }

    public static Map getAllSubTaskIssueTypes(boolean withBlankFirstEntry) {
        Map<String,String> rt = [:] as Map<String,String>
        if (withBlankFirstEntry) {
            rt.put("", "")
        }

        ComponentManager componentManager = ComponentManager.getInstance()
        ConstantsManager constantsManager = componentManager.getConstantsManager()
        constantsManager.getAllIssueTypeObjects().each {IssueType issueType ->
            if (issueType.isSubTask()) {
                rt.put(issueType.getId(), issueType.getName())
            }
        }

        rt
    }

    // Not used at the moment
    public static Map getAllWorkflowActions(String workflowNameOrNull) {
        Map<Integer,String> rt = [:] as Map<Integer,String>
        JiraWorkflow workflow = ComponentManager.getInstance().getWorkflowManager().getWorkflow(workflowNameOrNull)
        for (Object o in workflow.getAllActions()) {
            ActionDescriptor ad = (ActionDescriptor) o;
            rt.put(ad.getId(), "${ad.getName()} (${ad.getId()})")
        }
        rt.sort()
    }

    public static Map getAllLinkTypes(boolean withBlankFirstEntry) {
        Map<String,String> rt = [:] as Map<String,String>

        if (withBlankFirstEntry) {
            rt.put("", "")
        }

        IssueLinkTypeManager issueLinkTypeManager = ComponentManager.getComponentInstanceOfType(IssueLinkTypeManager.class) as IssueLinkTypeManager
        issueLinkTypeManager.getIssueLinkTypes().each {IssueLinkType issueLinkType ->
            rt.put(issueLinkType.getId() as String, issueLinkType.getName())
        }
        rt
    }

    public static Map getAllLinkTypesWithDirections(boolean withBlankFirstEntry) {
        Map<String,String> rt = [:] as Map<String,String>

        if (withBlankFirstEntry) {
            rt.put("", "")
        }

        IssueLinkTypeManager issueLinkTypeManager = ComponentManager.getComponentInstanceOfType(IssueLinkTypeManager.class) as IssueLinkTypeManager
        issueLinkTypeManager.getIssueLinkTypes().each {IssueLinkType issueLinkType ->
            // Don't bother with inward links for the moment - can't see the use right now
            rt.put("${issueLinkType.getId()} $OUTWARD_FIELD_NAME", issueLinkType.getOutward())
            rt.put("${issueLinkType.getId()} $INWARD_FIELD_NAME", issueLinkType.getInward())
        }
        rt
    }

    public static Map getAllOwnedFilters(boolean withBlankFirstEntry) {
        ComponentManager componentManager = ComponentManager.getInstance()
        SearchRequestService searchRequestService = componentManager.getSearchRequestService()
        JiraAuthenticationContext authenticationContext = componentManager.getJiraAuthenticationContext()

        Map<String,String> rt = [:] as Map<String,String>

        if (withBlankFirstEntry) {
            rt.put("", "")
        }
        searchRequestService.getOwnedFilters(authenticationContext.getUser()).each {SearchRequest sr ->
            rt.put(sr.id as String, sr.name)
        }
        rt
    }

    public static Map getAllFields (boolean withBlankFirstEntry) {
        // return a list of custom and system fields

        // No need to subtract the current field list? But it's acceptable to include the same field twice
        ComponentManager componentManager = ComponentManager.getInstance()
        FieldManager fieldManager = componentManager.getFieldManager()
        Set<Field> fields = fieldManager.getAllAvailableNavigableFields()

        Map<String,String> rt = [:] as Map<String,String>

        if (withBlankFirstEntry) {
            rt.put("", "")
        }

        fields.findAll {Field field ->
            !field.getName().startsWith("?")
        }
        .sort {Field a, Field b ->
            if (a.getName() && b.getName())
                return a.getName().compareToIgnoreCase(b.getName())
            else
                return -1
        }
        .each {
            rt.put(it.id, it.name)
        }
        rt
    }

    static boolean isSettableCFType(CustomField cf) {
        cf.getCustomFieldType() instanceof SelectCFType ||
            cf.getCustomFieldType() instanceof MultiSelectCFType ||
            cf.getCustomFieldType() instanceof CascadingSelectCFType
    }


    public static Map getFieldConfigSchemesByCustomField(boolean withBlankFirstEntry /*todo*/) {
        Map rt = [:]

        def customFieldManager = ComponentManager.getInstance().getCustomFieldManager()

        customFieldManager.getCustomFieldObjects().each {cf ->
            if (isSettableCFType(cf)) {
                Map configs = [:]
                cf.getConfigurationSchemes().each {fcs ->
                    configs.put(fcs.id, fcs.name)
                }
                rt.put(cf.name, configs)
            }
        }

        rt
    }
}