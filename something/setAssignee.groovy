import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.config.util.JiraHome
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.crowd.embedded.api.User
import groovy.json.*

IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
UserUtil userUtil = ComponentAccessor.getUserUtil()
import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.testAssignee")

MutableIssue issue = issueManager.getIssueObject("SAP-167")
MutableIssue issue2 = issueManager.getIssueObject("SAND-12")


CustomField escalationRecipientField = customFieldManager.getCustomFieldObject("customfield_15952") // Escalation Recipient
CustomField escalationDescriptionField = customFieldManager.getCustomFieldObject("customfield_15160") // Escalation Description
CustomField daysInStatusField = customFieldManager.getCustomFieldObject("customfield_15275") // Days in Status
CustomField ITManagerField = customFieldManager.getCustomFieldObject("customfield_14067") // ITManager

String fileName = ComponentAccessor.getComponentOfType(JiraHome.class).getHome().toString() + "/scripts/SAP-role-mappings" + "/sap-escalation-assignee.json"
File file = new File(fileName)

String issuetype = issue.issueTypeObject.name
String status = issue.statusObject.name
String priority = issue.getPriorityObject().getName()
String component = issue.getComponentObjects().getAt(0)?.getName()

String ITDeliveryManagerID
String ITDeliveryManagerDisplayName
String ITManagerID
String ITManagerDisplayName
String ITSAPOwnerID
//String daysInStatus = String.format("%d", (long) issue.getCustomFieldValue(daysInStatusField))

if (file) {
    def slurper = new JsonSlurper()
	def arr = slurper.parseText(file.text)
	if (arr["IT Delivery Manager"]) {
    	ITDeliveryManagerID = arr["IT Delivery Manager"][component]
    }  
	if (arr["IT SAP Owner"]) {
    	ITSAPOwnerID = arr["IT SAP Owner"][component]
    }    
}

ApplicationUser ITManagerUser = issue.getCustomFieldValue(ITManagerField) as ApplicationUser
log.error "ITManagerUser: " + ITManagerUser
User assigneeUser = userUtil.getUserObject(ITManagerUser.getName())
log.error "assigneeUser: " + assigneeUser
issue2.setAssignee(userUtil.getUserObject(ITManagerUser.getName()))
log.error "New setAssignee: " + issue2.getAssigneeId()
log.error "Key: " + issue2.getKey()
/*
if (ITManagerUser){
	ITManagerID = ITManagerUser.name 
	ITManagerDisplayName = ITManagerUser.displayName
} else {
	ITManagerDisplayName = "unassigned"
}

ApplicationUser ITDeliveryManagerUser = ComponentAccessor.getUserUtil().getUserByKey(ITDeliveryManagerID)
ITDeliveryManagerDisplayName = ITDeliveryManagerUser.displayName

Option escalationRecipientValue = issue.getCustomFieldValue(escalationRecipientField) as Option;
 
String escalationRecipientString = escalationRecipientValue.getValue(); 

if (escalationRecipientString == "IT Delivery Manager"){
	issue.setAssigneeId(ITDeliveryManagerID)
}
else if (escalationRecipientString == "IT Manager"){
	if (ITManagerUser) {
		issue.setAssigneeId(ITManagerID)}
	else {
		issue.setAssigneeId(ITDeliveryManagerID)
	}
}
else if (escalationRecipientString == "IT SAP Owner"){
	issue.setAssigneeId(ITSAPOwnerID)
}
*/
/*
String escalationDescriptionValue = issue.getCustomFieldValue(escalationDescriptionField) as String;
escalationDescriptionValue = escalationDescriptionValue + "\n-----"
escalationDescriptionValue = escalationDescriptionValue + "\nDays on last Status (" + status + "): " + daysInStatus
escalationDescriptionValue = escalationDescriptionValue + "\nIssue Type: " + issuetype
escalationDescriptionValue = escalationDescriptionValue + "\nPriority: " + priority
escalationDescriptionValue = escalationDescriptionValue + "\nIT Delivery Manager: " + ITDeliveryManagerDisplayName
escalationDescriptionValue = escalationDescriptionValue + "\nIT Manager: " + ITManagerDisplayName
issue.setCustomFieldValue(escalationDescriptionField, escalationDescriptionValue);
*/