import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.customfields.manager.OptionsManager
import com.atlassian.jira.issue.fields.config.FieldConfig

import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
log.debug("Start set Hold On operation : " + issue.key)

String linkname="Created on transition"

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder()

def customFieldManager = ComponentManager.getInstance().getCustomFieldManager()
def cf = customFieldManager.getCustomFieldObjectByName("Hold On")

issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()

for (IssueLink link in issueLinkManager.getInwardLinks(issue.getId())){
	log.debug("Check links by name : " + link.getIssueLinkType().getName())
	if (link.getIssueLinkType().getName()==linkname){
		//link.getSourceObject()
        log.debug("Linked issue with Created on transition relation :" + link.getSourceObject().getKey())
        log.debug("Actual Hold On field value :" + link.getSourceObject().getCustomFieldValue(cf))
		def fieldConfig = cf.getRelevantConfig(link.getSourceObject())
        OptionsManager optionsManager = ComponentManager.getComponentInstanceOfType(OptionsManager.class)
        // Unset value - use empy Array like ArrayList yes = []
        //ArrayList yes = []
		ArrayList yes = [ optionsManager.getOptions(fieldConfig).getOptionForValue("On Hold",null)]
		log.debug("update linked issue Hold On value :" + yes)
		ModifiedValue mVal = new ModifiedValue(link.getSourceObject().getCustomFieldValue(cf), yes )
		//log.error("Update sub-tasks field - Modified value " + mVal)
		cf.updateValue(null, link.getSourceObject(), mVal, changeHolder)
	}
}