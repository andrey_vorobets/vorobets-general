import com.atlassian.jira.project.Project
import com.atlassian.jira.project.ProjectManager
import org.apache.log4j.Category
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.bc.project.component.ProjectComponent
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
log.debug("Created issue key: " + issue.key)

ComponentManager componentManager = ComponentManager.getInstance()
Project project = issue.getProjectObject()
log.debug("Created issue key: get project object ")
ProjectComponent component = componentManager.getProjectComponentManager().findByComponentName(project.getId(), "Releases")
log.debug("Created issue key: get component object ")
issue.setComponents([component.getGenericValue()])
log.debug("Created issue key: set component")