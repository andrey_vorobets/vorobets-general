import javax.naming.InitialContext
import groovy.sql.Sql
import javax.naming.Context
import javax.sql.DataSource

import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")
// Feature / Bug - work as file only - doesn't work from COnsole

log.error "Something"
String dor_res = ""
try {
    // Get Context to connect to database as a datasource
    Context ctx = new InitialContext()
    if (ctx == null) {
        log.error "No DS"
        throw new Exception("No Context found!")
    }
    DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/Crowd")
    log.error ds
    if (ds) {
        def sql = new Sql(ds)
        def sqlStmt = "select user_name from cwd_user limit 5"
        
        sql.eachRow(sqlStmt) {row ->
        	log.error row
			dor_res = row.toString()
		}
		sql.close()
    }
} catch (all) {
    log.error "Error" + all
}
