import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.config.SubTaskManager
import com.atlassian.crowd.embedded.api.User

import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.ComponentManager

import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager

import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.link.IssueLinkManager

import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

IssueManager issueManager = ComponentAccessor.getIssueManager()
//MutableIssue issue = issueManager.getIssueObject("ISADM-653")

log.debug("Link by User_s Identifier: " + issue.key)

def constantManager = ComponentAccessor.getConstantsManager()
def issueService = ComponentAccessor.getIssueService()
ComponentManager componentManager = ComponentManager.getInstance()
User cwdUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()

String linkname="Relates"
// Master project - project where post function was added
String master_projectkey = "ISADM"
// Issue type for project where we add post function to the workflow & from which issue link will be added to the User profile issue
String master_project_issuetype = "Purchase"

// Project & issuetype where User profile is. Used for search & link to
String projectkey = "ISSEC"
String project_issuetype = "User profile"
// Custom field for search - User(s) Identifier, For whom or whatever users based field
String ForWhom = "customfield_11111"
// User dependencies link
Long link_id = 10600

if (issue.getProjectObject().getKey() == master_projectkey && issue.getIssueTypeObject().getName()==master_project_issuetype) {
	CustomField cf= customFieldManager.getCustomFieldObject(ForWhom)
	//log.debug("Link Users Identifier Users field value:" + issue.getCustomFieldValue(cf).toString())
	issue.getCustomFieldValue(cf).each {
		// it - com.atlassian.crowd.embedded.api.User
		log.debug("Link Users Identifier Users field value:" + it.getDisplayName())
		jqlSL = ["project="+projectkey+" and \"User Identifier\" = "+it.getName()+" and issuetype=\""+project_issuetype+"\""]
		jqlSL.each {
			//log.debug "Users identifier search JQL String: " + it
			SearchService.ParseResult parseResult = searchService.parseQuery(cwdUser, it)
			if (parseResult.isValid()) {
				def searchResult = searchService.search(cwdUser, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
				issues = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
				//log.debug "Users identifier search JQL Result Map size: " + issues.size()
				if (issues.size()>0) {
					try {
						/*
						for (sissue in issues) {							
							issueLinkManager.createIssueLink(issue.getId(), sissue.getId(), link_id, null, cwdUser)
						}
						*/
						issues.eachWithIndex { it2, i2 ->
							issueLinkManager.createIssueLink(issue.getId(), it2.getId(), link_id, null, cwdUser)
						}
					} catch (all) {
						log.debug ("Users identifier link - something wrong in try to Link")
					}
				}
			} else {
		   			log.debug("Invalid JQL: " + it)
		    }
		}
	}
}