import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.config.SubTaskManager
import com.atlassian.crowd.embedded.api.User

import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.ComponentManager

import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager

import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

IssueManager issueManager = ComponentAccessor.getIssueManager()
//MutableIssue issue = issueManager.getIssueObject("ISADM-649")
//MutableIssue issue = issue
log.debug("Resole linked issue: " + issue.key)

//Issue issue = issue
def constantManager = ComponentAccessor.getConstantsManager()
def issueService = ComponentAccessor.getIssueService()
//def cwdUser = ComponentAccessor.jiraAuthenticationContext.getUser()
//def cwdUser = ComponentAccessor.getJiraAuthenticationContext().getUser()
User cwdUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

String linkname="Relates"
String projectkey = "ISADM"
String project_issuetype = "Purchase"
//String find_str = "Done"

if (issue.getProjectObject().getKey() == projectkey && issue.getIssueTypeObject().getName()==project_issuetype) {
	issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()
	for (IssueLink link in issueLinkManager.getInwardLinks(issue.getId())){
		log.debug("Check links by name : " + link.getIssueLinkType().getName())
		if (link.getIssueLinkType().getName()==linkname){
			log.debug("Linked issue with Relates link :" + link.getSourceObject().getKey())
			String comment = "*Resolving* as a result of the *Resolve* action being applied to the outcome.";
			def issueInputParameters = new IssueInputParametersImpl()
			def doneResolutionId = constantManager.getResolutionObjects().find {it.name == "Done"}?.id
			def doneStatusId = constantManager.getStatusObjects().find {it.name == "Done"}?.id
			issueInputParameters.setResolutionId(doneResolutionId).setStatusId(doneStatusId).setComment(comment)
			def transitionValidationResult = issueService.validateTransition(cwdUser, link.getSourceObject().id, 801, issueInputParameters)
			// 801 - workflow Action
			log.debug("Validation result : ," + transitionValidationResult.errorCollection)
			if (transitionValidationResult.isValid()) {
            	log.debug("Transition for issue " + link.getSourceObject().key  + " is valid")
            	issueService.transition(cwdUser, transitionValidationResult)
        	} else {
        		transitionValidationResult = issueService.validateTransition(cwdUser, link.getSourceObject().id, 761, issueInputParameters)
        		log.debug("Validation result : ," + transitionValidationResult.errorCollection)
        		if (transitionValidationResult.isValid()) {
        			log.debug("Transition for issue " + link.getSourceObject().key  + " is valid")
        			issueService.transition(cwdUser, transitionValidationResult)
        		}
        	}
		}
	}
}