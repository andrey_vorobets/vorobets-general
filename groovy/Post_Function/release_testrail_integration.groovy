import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkManager

import groovy.json.JsonSlurper
import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
log.debug "Test integration with Test Rail " + issue

String linkname="Relates"
String credStr = "user:password".getBytes().encodeBase64().toString()
String urlRoot = "http://8.8.8.8/testrail/index.php?"

def slurper = new JsonSlurper()
ComponentManager componentManager = ComponentManager.getInstance()
issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()
def IssuesMap = []
int link_counter = 0

def parseJSON(text, slurper)
{
    def result = slurper.parseText(text)
    return result
}

def getResult(addr, credStr, slurper) {
    def conn = addr.toURL().openConnection()
    conn.setRequestProperty( "Authorization", "Basic ${credStr}" )
    conn.setRequestProperty( "Content-Type", "application/json" )
    conn.connect()
    // log.debug("TestRail: getResult responce code : " + conn.responseCode)
    if( conn.responseCode == 200 ) {
        def result = parseJSON(conn.content.text, slurper)
        return result
    }
}

def checkRefs(IssuesMap, RefMap, issuekey) {
	def result = 0
    if (RefMap.indexOf(issuekey) >=0 ){
        return 0
    }
	RefMap.each {
		if (IssuesMap.indexOf(it) >= 0){
			result = 1
		}
	}
	return result
}

def updateCase(addr, refs, credStr) {
	def conn = addr.toURL().openConnection()
	conn.setRequestProperty( "Authorization", "Basic ${credStr}" )
	conn.setRequestProperty( "Content-Type", "application/json" )
	conn.setRequestMethod("POST")
	conn.doOutput = true
	def writer = new OutputStreamWriter(conn.outputStream)
	def queryString = "{\"refs\": \""+refs+"\"}"
	writer.write(queryString)
	writer.flush()
	writer.close()
	conn.connect()
	if( conn.responseCode == 200 ) {
		def response = conn.content.text
		log.debug("TestRail: Update Case response : " + response)
	}
}

for (IssueLink link in issueLinkManager.getOutwardLinks(issue.getId())){
	//log.debug("TestRail: Link by name : " + link.getIssueLinkType().getName())
    if (link.getIssueLinkType().getName()==linkname){
    	log.debug("TestRail: Linked Issue " + link.getDestinationObject().getKey().toString())
    	IssuesMap.add(link.getDestinationObject().getKey().toString())
        link_counter=link_counter+1
        }
}
log.debug("TestRail: Found Relates links:" + link_counter)
log.debug("TestRail: Issues Map:" + IssuesMap)

// get All projects
def casesMap = []
def proj_addr       = "${urlRoot}/api/v2/get_projects"
log.debug("TestRail: Projects Rest URL:" + proj_addr)
def result = getResult(proj_addr, credStr, slurper)
def checkRefsInIssues = 0
//log.defug "json :${result}"
// index i if you whant to count how many projects you have
result.eachWithIndex { it, i ->
    //log.defug "$i: $it"
    log.debug "TestRail: Project ID:" + it.get("id")
    def proj_id=it.get("id")

    // Suites
    def suites = "${urlRoot}/api/v2/get_suites/${proj_id}"
    log.debug "TestRail: suites rest url: " + suites
    def suit_result=getResult(suites, credStr, slurper)
    //log.defug "json :${suit_result}"
    suit_result.eachWithIndex { it2, i2 ->
        //log.debug "TestRail: Suite ID: " + it2.get("id")
        def suite_id = it2.get("id")
        def cases = "${urlRoot}/api/v2/get_cases/${proj_id}&suite_id=${suite_id}"
        //log.debug "TestRail: Case ID: " + cases
        def cases_result=getResult(cases, credStr, slurper)
        //log.defug "json :${cases_result}"
        cases_result.eachWithIndex { it3, i3 ->
            def refs = it3.get("refs")
            if (refs.toString()!="null") {
                //log.defug "json :${cases_result}"
                //log.debug "TestRail: Case ID: " + it3.get("id") + " Refs: " + refs
                //log.debug "TestRail: Case Refs tokenize: " + refs.tokenize(", ")
                checkRefsInIssues = checkRefs(IssuesMap, refs.tokenize(", "), issue.key)
                //log.debug "TestRail: Case Ref presence in Issues Map: " + checkRefsInIssues
                if (checkRefsInIssues==1){
                	log.debug "TestRail: Case ID: " + it3.get("id") + " Refs: " + refs
                	//log.debug "TestRail: Case Refs tokenize: " + refs.tokenize(", ")
                	def updateCaseUrl = "${urlRoot}/api/v2/update_case/"+it3.get("id")
                	def newRefs = refs + ", "+issue.key
                	log.debug "New refs: " + newRefs + " ;URL " + updateCaseUrl
                	updateCase(updateCaseUrl, newRefs, credStr)
                }
            }
        }
    }
}