// Set pre-defined assignee for the pre-defined issue type and project
import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.util.UserUtil

UserUtil userUtil = ComponentAccessor.getUserUtil()

String asUserName = "avorobets"
log.debug "MKJ create issue project key: " + issue.getProjectObject().getKey() + " Issue Type: " + issue.getIssueTypeObject().getName()

if (issue.getProjectObject().getKey()=="MKJ" && issue.getIssueTypeObject().getName()=="Bug") {
	user = userUtil.getUserObject(asUserName)
	issue.setAssignee(user)
}