import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.link.IssueLink
import org.apache.log4j.Category
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.crowd.embedded.api.User

String bundle_field_id = "customfield_10902"
Long link_id = 10003

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
log.debug("Bundle link issue key: " + issue.key)

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject(bundle_field_id) 

def value = issue.getCustomFieldValue(cf)
log.debug("Bundle link value of bundle: " + value)
String jql_query="issue in fixedInBundle("
for (String dd in value.tokenize("###")) {
    log.debug("Bundle link value of budle id: " + dd.tokenize(". ")[0])
    jql_query=jql_query+dd.tokenize(". ")[0]+","
}
jql_query=jql_query[0..-2]+")"
log.debug("Bundle link JQL query: " + jql_query)

def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
SearchProvider searchProvider = ComponentAccessor.getComponentOfType(SearchProvider.class)
def issueManager = ComponentAccessor.getIssueManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getUser()

def query = jqlQueryParser.parseQuery(jql_query)
SearchResults results = searchProvider.search(query, user, PagerFilter.getUnlimitedFilter())
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
currentUser = authenticationContext.getLoggedInUser()
results.getIssues().each {documentIssue ->
    // if you need a mutable issue you can do:
    def fissue = issueManager.getIssueObject(documentIssue.id)
    // do something to the issue...
    //log.error(fissue.summary)
    issueLinkManager.createIssueLink(issue.getId(), fissue.getId(), link_id, null, currentUser)
}