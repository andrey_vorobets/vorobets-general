import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
log.debug("Grab configuration notes issue key : " + issue.key)

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

linkMgr = ComponentManager.getInstance().getIssueLinkManager()

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()

CustomField cf= customFieldManager.getCustomFieldObject("customfield_10502")
CustomField confnt= customFieldManager.getCustomFieldObject("customfield_10903")
String confnotes = ""

for (IssueLink link in linkMgr.getOutwardLinks(issue.id)) {
 
 def labEq = link.getDestinationObject().getCustomFieldValue(cf).toString()
    if (labEq!="null") {
 			confnotes = confnotes + "\\[" + link.getDestinationObject().getKey().toString() + "\\] " + link.getDestinationObject().getSummary()+ "\n\n" + labEq + "         \n\n"
        }
}
issue.setCustomFieldValue(confnt, confnotes)