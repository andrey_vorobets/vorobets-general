import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.util.JiraDurationUtils

import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

JiraDurationUtils jiraDurationUtils = ComponentAccessor.getJiraDurationUtils()
String rawLog = issue.getModifiedFields().get("worklog").getNewValue().timeLogged()

log.error "Converted: " + jiraDurationUtils.parseDuration(rawLog)