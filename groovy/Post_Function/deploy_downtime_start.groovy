// Create start session for Deploy downtime
// "insert into deploy_downtime (issueid, starttime) values ('ISSUEID', now())"
import org.apache.log4j.Category

import com.atlassian.jira.ComponentManager

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

ComponentManager componentManager = ComponentManager.getInstance()
delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");
Connection conn = ConnectionFactory.getConnection(helperName);

Sql sql = new Sql(conn)
def sqlStmt = "insert into deploy_downtime (issueid, starttime) values ('"+issue.id+"', now())"
log.debug ("Downtime start button PostFunc: SQL query: " + sqlStmt)

sql.execute sqlStmt

sql.close()