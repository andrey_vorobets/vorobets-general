import com.atlassian.jira.component.ComponentAccessor
 
def userManager = ComponentAccessor.getUserManager()
def groupManager = ComponentAccessor.getGroupManager()
def issueManager = ComponentAccessor.getIssueManager()
def watcherManager = ComponentAccessor.getWatcherManager()
def group = userManager.getGroup("department-s2-all")
 
groupManager.getUsersInGroup(group).each {
    watcherManager.startWatching(it, issue)
}