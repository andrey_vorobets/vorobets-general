import org.apache.log4j.Category
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()

CustomField startf= customFieldManager.getCustomFieldObject("customfield_10703")
CustomField endf= customFieldManager.getCustomFieldObject("customfield_10704")
String linkname="IM / PM process"

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

String projectkey = "ITSERV"
String project_issuetype = "Problem"

log.debug("Set Time in LinkedIssues")

//MutableIssue issue = issueManager.getIssueObject("SAND-21")
//MutableIssue issue = issueManager.getIssueObject("ITSERV-4313")
log.debug("Set Time in LinkedIssues - problem issue key: " + issue.key)

//def startfieldConfig = startf.getRelevantConfig(issue)
log.debug("Set Time in LinkedIssues - Start Time: " + issue.getCustomFieldValue(startf).toString() )
//log.debug("Set Time in LinkedIssues - Start Time lenght: " + issue.getCustomFieldValue(startf).toString().length() )
log.debug("Set Time in LinkedIssues - End Time: " + issue.getCustomFieldValue(endf).toString() )

if (issue.getProjectObject().getKey() == projectkey && issue.getIssueTypeObject().getName()==project_issuetype) {
	for (IssueLink link in issueLinkManager.getInwardLinks(issue.getId())){
		log.debug("Check links by name : " + link.getIssueLinkType().getName())		
		if (link.getSourceObject().getIssueTypeObject().getName()=="Incident") {
			log.debug("Set Time in LinkedIssues - Destination Object: " + link.getSourceObject().getIssueTypeObject().getName())
			if (issue.getCustomFieldValue(endf)!=null) {
				ModifiedValue BVal = new ModifiedValue(link.getSourceObject().getCustomFieldValue(endf), issue.getCustomFieldValue(endf))
				endf.updateValue(null, link.getSourceObject(), BVal, changeHolder)
			}
			if (issue.getCustomFieldValue(startf)!=null) {
				ModifiedValue BVal = new ModifiedValue(link.getSourceObject().getCustomFieldValue(startf), issue.getCustomFieldValue(startf))
				startf.updateValue(null, link.getSourceObject(), BVal, changeHolder)
			}
		}
	}
}