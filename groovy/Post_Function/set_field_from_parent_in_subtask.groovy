import org.apache.log4j.Category
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder
/*

Post Function: Set custom field (select) from parent in sub-task
Put at Create Issue Transition, require follow order of Post-Functions:
  Creates the issue originally.
Script workflow function : A custom script will be run:
from inline script.
Stores updates to an issue (no change history is created).
Re-index an issue to keep indexes in sync with the database. 
*/

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject("customfield_10306")
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

log.debug("Update sub-tasks field")

log.debug("Update sub-tasks field - created issue key: " + issue.key)
if (issue.isSubTask()) {
 def parentIssue = issue.getParentObject();
 log.debug("Update sub-tasks field - Parent issue key: " + parentIssue.getKey())
        def sq = parentIssue.getCustomFieldValue(cf).toString()
        log.debug("Update sub-tasks field - Parent field value " + sq.toString())

        if (sq.toString()!="null") {
     Options options = cf.getOptions(null, cf.getRelevantConfig(issue), null)
         log.debug("Update sub-tasks field - options " + options)

     Option newOption = options.getOptionForValue(sq, null)
     log.debug("Update sub-tasks field - need option " + newOption)
     ModifiedValue mVal = new ModifiedValue(issue.getCustomFieldValue(cf), newOption )
            log.debug("Update sub-tasks field - Modified value " + mVal)

     cf.updateValue(null, issue, mVal, changeHolder)
       } else {
         log.debug("Update sub-tasks field - empty parent field")
      }
}
else {
    log.debug("Update sub-tasks field - issue is not sub-task")
}