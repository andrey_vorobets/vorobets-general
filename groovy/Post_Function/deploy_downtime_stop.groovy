// Set stop session for Deploy downtime
// "update deploy_downtime set endtime=now() where id=(select id from deploy_downtime where issueid='ISSUEID' and endtime is null order by id desc limit 1)"
import org.apache.log4j.Category

import com.atlassian.jira.ComponentManager

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

ComponentManager componentManager = ComponentManager.getInstance()
delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");
Connection conn = ConnectionFactory.getConnection(helperName);

Sql sql = new Sql(conn)
def sqlStmt = "update deploy_downtime set endtime=now() where id=(select id from deploy_downtime where issueid='"+issue.id+"' and endtime is null order by id desc limit 1)"
log.debug ("Downtime stop button PostFunc: SQL query: " + sqlStmt)

sql.execute sqlStmt

sql.close()