import org.apache.log4j.Category
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
log.debug("S2 start datetime issue : " + issue.key)

ComponentManager componentManager = ComponentManager.getInstance()
cfm = componentManager.getCustomFieldManager()
ch  = new DefaultIssueChangeHolder()
cfo= cfm.getCustomFieldObject("customfield_11300")

t0  = new java.sql.Timestamp(System.currentTimeMillis())
log.debug "StartTime => '"+t0.toString()+"'"

mv = new ModifiedValue(cfo, t0)
ch = new DefaultIssueChangeHolder()
cfo.updateValue(null, issue, mv, ch)

log.debug("end")