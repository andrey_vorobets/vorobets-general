/* Open transition - check "User Identifier" field. If not empty, check ITENT project
issue number should be 1 or 0
In case of 1 - Link issue to the found issue as "Relates"
*/
import org.apache.log4j.Category

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.link.IssueLinkManager

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.web.bean.PagerFilter

String userIdent = "customfield_11104"
String adminUserName = "avorobets"
Long link_id = 10003

def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")
log.debug("ITENT ISSEC integration start: " + issue.key)

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField uifield= customFieldManager.getCustomFieldObject(userIdent)
SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
UserUtil userUtil = ComponentAccessor.getUserUtil()
IssueManager issueManager = componentManager.getIssueManager()
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()

User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
if (!user) {
user = userUtil.getUserObject(adminUserName)
}
String searchKey = ""

log.debug "ITENT ISSEC integration - UI field: " + issue.getCustomFieldValue(uifield).getName()
if (issue.getCustomFieldValue(uifield).getName()!="null") {
	jqlSL = ["project=ITENT and issuetype = \"User profile\" and \"User Identifier\"="+issue.getCustomFieldValue(uifield).getName()]
	log.debug "ITENT ISSEC integration - jqlSL: " + jqlSL
	jqlSL.each {
		SearchService.ParseResult parseResult = searchService.parseQuery(user, it)
		if (parseResult.isValid()) {
			def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
			issues = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
			if (issues.size()==1) {
				for ( sissue in issues ) {
					searchKey = sissue.getKey()
					log.debug "ITENT ISSEC integration - founded key: " + searchKey
					issueLinkManager.createIssueLink(issue.getId(), sissue.getId(), link_id, null, user)
				}
			}
		} else {
	   		log.debug("Invalid JQL: " + it)
	    }
	}
}