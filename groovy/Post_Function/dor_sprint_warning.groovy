import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

import com.atlassian.jira.issue.ModifiedValue
//import com.atlassian.jira.issue.customfields.option.Options
//import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

//import com.atlassian.jira.component.ComponentAccessor

import org.apache.log4j.Category

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

// DoR Warning
// In dev 11700, prod 11006
String DoRWarning = "customfield_11700"

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField tgtField= customFieldManager.getCustomFieldObject(DoRWarning) 

log.error "DoR Issue Created " + issue
tgtField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(tgtField), "DoR is not filled, issue cannot be added to the sprint"),changeHolder)