//package com.onresolve.jira.groovy.canned.workflow.postfunctions
package com.onresolve.scriptrunner.canned.jira.workflow.postfunctions

import com.onresolve.scriptrunner.canned.util.BuiltinScriptErrors
import com.onresolve.scriptrunner.canned.CannedScript
//import com.onresolve.jira.groovy.canned.CannedScript
import com.opensymphony.util.TextUtils
//import com.onresolve.jira.groovy.canned.utils.ConditionUtils
import com.onresolve.scriptrunner.canned.jira.utils.ConditionUtils
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.util.ErrorCollection
import com.atlassian.jira.util.SimpleErrorCollection
import org.apache.log4j.Logger

class SimplePostfunction implements CannedScript{

    Logger log = Logger.getLogger(SimplePostfunction.class)
    public static final String FIELD_ADDITIONAL_SCRIPT = 'FIELD_ADDITIONAL_SCRIPT'

    String getName() {
        "Simple scripted postfunction"
    }

    public String getHelpUrl() {
        ""
    }

    String getDescription() {
        "Runs a simple embedded script"
    }

    List getCategories() {
        ["Function"]
    }


    List getParameters(Map params) {
        [
                ConditionUtils.getConditionParameter(),
                [
                        Name:FIELD_ADDITIONAL_SCRIPT,
                        Label:'Script',
                        Description:"Enter any customisations to the issue, e.g. hard-coding specific field values.",
                        Type: "mediumtext",
                        Examples: [
                                "Set issue summary": "issue.summary = \\'Cloned issue\\'",
                                "Set custom field": "def cf = customFieldManager.getCustomFieldObjects(issue).find {it.name == \\'MyCustomFieldType\\'}\\n" +
                                        "issue.setCustomFieldValue(cf, \\'my value\\')"
                        ]
                ]
        ]

    }

    public ErrorCollection doValidate(Map params, boolean forPreview) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection()
        if (! params[FIELD_ADDITIONAL_SCRIPT] ) {
            errorCollection.addError(FIELD_ADDITIONAL_SCRIPT, "You must provide an additional script.")
        }
        errorCollection
    }

    Map doScript(Map params) {
        MutableIssue issue = params['issue'] as MutableIssue
        Map transientVars = params['transientVars'] as Map

        Boolean doIt = ConditionUtils.processCondition(params[ConditionUtils.FIELD_CONDITION] as String, issue, false, params)
        if (! doIt) {
            return [:]
        }

        ConditionUtils.doAdditional(params[FIELD_ADDITIONAL_SCRIPT] as String, issue, [transientVars: transientVars])

        [:]
    }

    String getDescription(Map params, boolean forPreview) {
        getName() +  " : Condition: <pre>" + TextUtils.htmlEncode(params[ConditionUtils.FIELD_CONDITION] as String) + "</pre> Script: <pre>" + TextUtils.htmlEncode(params[FIELD_ADDITIONAL_SCRIPT] as String) + "</pre>"
    }

    public Boolean isFinalParamsPage(Map params) {
        true
    }

}