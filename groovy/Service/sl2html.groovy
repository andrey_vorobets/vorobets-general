/*
class: com.onresolve.jira.groovy.GroovyService
Path in dev:  /opt/atlassian/jira.home.6412/scripts
Path in Prod: /opt/jira-test-home/scripts
*/

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.ManagerFactory
import org.apache.log4j.Category

import javax.mail.*
import javax.mail.internet.*
import javax.activation.*

Category log = Category.getInstance("com.onresolve.jira.groovy")
log.setLevel(org.apache.log4j.Level.DEBUG)

log.debug "SL2 HTML Service Debug"

//Prod SL2
jqlSL = ['project=INCPORT and priority=Critical and issueFunction in lastComment("before -1d") and status = "Waiting for support"', 'project=INCPORT and priority=Major and issueFunction in lastComment("before -2d") and status = "Waiting for support"', 'project=INCPORT and priority=Minor and issueFunction in lastComment("before -4d") and status = "Waiting for support"', 'project=INCPORT and priority=Trivial and issueFunction in lastComment("before -7d") and status = "Waiting for support"']

def String adminUserName = "avorobets"
def toAddress = "vorobets@gmail.com"
def slAddress = "xxx@xxx.com"
def fromAddress = "jira@myserver.com"
def host = "localhost"
def port = "25"

def sendmail(String message ,String subject, String toAddress, String fromAddress, String host, String port){
   Properties mprops = new Properties();
   mprops.setProperty("mail.transport.protocol","smtp");
   mprops.setProperty("mail.host",host);
   mprops.setProperty("mail.smtp.port",port);
 
   Session lSession = Session.getDefaultInstance(mprops,null);
   MimeMessage msg = new MimeMessage(lSession);
 
   StringTokenizer tok = new StringTokenizer(toAddress,";");
   ArrayList emailTos = new ArrayList();
   while(tok.hasMoreElements()){
      emailTos.add(new InternetAddress(tok.nextElement().toString()));
   }
   InternetAddress[] to = new InternetAddress[emailTos.size()];
   to = (InternetAddress[]) emailTos.toArray(to);
   msg.setRecipients(MimeMessage.RecipientType.TO,to);
   msg.setFrom(new InternetAddress(fromAddress));
   msg.setSubject(subject);
   //msg.setText(message)
   msg.setContent(message, "text/html; charset=utf-8")
 
   Transport transporter = lSession.getTransport("smtp");
   transporter.connect();
   transporter.send(msg);
}

SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
UserUtil userUtil = ComponentAccessor.getUserUtil()
User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = componentManager.getIssueManager()
 
if (!user) {
user = userUtil.getUserObject(adminUserName)
}

List<Issue> issues = null

String subject = ""
String cssclass="<style type=\"text/css\">.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}.tftable tr {background-color:#d4e3e5;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}.tftable tr:hover {background-color:#ffffff;}</style>"
String body = cssclass+"<table class=\"tftable\" border='1'><tr><td>Priority</td><td>Reporter</td><td>Last Update</td><td>Summary</td><td>URL</td></tr>"

subject = "Escalation: Jira Waiting For Support review"
log.debug "subject: " + subject

jqlSL.each {
	SearchService.ParseResult parseResult = searchService.parseQuery(user, it)
	if (parseResult.isValid()) {
		def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
		issues = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
		for ( issue in issues ){
			body=body+"<tr><td>"+issue.getPriorityObject().getName()+"</td><td>"+issue.getReporterUser().getDisplayName()+"</td><td>"+issue.getUpdated().toString()[0..15]+"</td><td>"+issue.getSummary()+"</td><td>"+"<a href=\"https://jira.internal-services.com/browse/"+issue.getKey()+"\">"+issue.getKey()+"</a></td></tr>"
		}
	} else {
   		log.debug("Invalid JQL: " + it)
    }
}
body=body+"</table>"
log.debug("Body " + body)
sendmail(body , subject, toAddress, fromAddress, host, port)
sendmail(body , subject, slAddress, fromAddress, host, port)