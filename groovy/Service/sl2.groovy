/*
class: com.onresolve.jira.groovy.GroovyService
Path in dev:  /opt/atlassian/jira.home.6412/scripts
Path in Prod: /opt/jira-test-home/scripts
*/

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.ManagerFactory
import org.apache.log4j.Category

import javax.mail.*
import javax.mail.internet.*
import javax.activation.*
// Prod Customer
jqlSearch = ['project=INCPORT and priority=Critical and issueFunction in lastComment("before -1d") and status = "Waiting for customer"', 'project=INCPORT and priority=Major and issueFunction in lastComment("before -2d") and status = "Waiting for customer"', 'project=INCPORT and priority=Minor and issueFunction in lastComment("before -4d") and status = "Waiting for customer"', 'project=INCPORT and priority=Trivial and issueFunction in lastComment("before -7d") and status = "Waiting for customer"']
// Test Customer
//jqlSearch = ['project=INCPORT and priority=Critical and issueFunction in lastComment("before -1d") and status = "Waiting for customer"', 'project=INCPORT and priority=Major and issueFunction in lastComment("before -2d") and status = "Waiting for customer" and key="INCPORT-930"']

//Prod SL2
jqlSL = ['project=INCPORT and priority=Critical and issueFunction in lastComment("before -1d") and status = "Waiting for support"', 'project=INCPORT and priority=Major and issueFunction in lastComment("before -2d") and status = "Waiting for support"', 'project=INCPORT and priority=Minor and issueFunction in lastComment("before -4d") and status = "Waiting for support"', 'project=INCPORT and priority=Trivial and issueFunction in lastComment("before -7d") and status = "Waiting for support"']

def String adminUserName = "avorobets"
def toAddress = "vorobets@gmail.com"
def slAddress = "xxx@xxx.com"
def fromAddress = "jira@myserver.com"
def host = "localhost"
def port = "25"

def sendmail(String message ,String subject, String toAddress, String fromAddress, String host, String port){
   Properties mprops = new Properties();
   mprops.setProperty("mail.transport.protocol","smtp");
   mprops.setProperty("mail.host",host);
   mprops.setProperty("mail.smtp.port",port);
 
   Session lSession = Session.getDefaultInstance(mprops,null);
   MimeMessage msg = new MimeMessage(lSession);
 
   StringTokenizer tok = new StringTokenizer(toAddress,";");
   ArrayList emailTos = new ArrayList();
   while(tok.hasMoreElements()){
      emailTos.add(new InternetAddress(tok.nextElement().toString()));
   }
   InternetAddress[] to = new InternetAddress[emailTos.size()];
   to = (InternetAddress[]) emailTos.toArray(to);
   msg.setRecipients(MimeMessage.RecipientType.TO,to);
   msg.setFrom(new InternetAddress(fromAddress));
   msg.setSubject(subject);
   msg.setText(message)
   // message.setContent(message, "text/html; charset=utf-8")
 
   Transport transporter = lSession.getTransport("smtp");
   transporter.connect();
   transporter.send(msg);
}

Category log = Category.getInstance("com.onresolve.jira.groovy")
log.setLevel(org.apache.log4j.Level.DEBUG)

log.debug "SL2 Service Debug"


SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
UserUtil userUtil = ComponentAccessor.getUserUtil()
User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = componentManager.getIssueManager()
 
if (!user) {
user = userUtil.getUserObject(adminUserName)
}

List<Issue> issues = null

String subject = ""
String body = ""
// /*
jqlSearch.each {
	SearchService.ParseResult parseResult = searchService.parseQuery(user, it)
	if (parseResult.isValid()) {
		def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
		issues = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
		for ( issue in issues ){
			//log.debug "issue: ${issue.getId()}, ${issue.getKey()}, ${issue.getReporterUser().getEmailAddress()}, ${issue.getReporterUser().getDisplayName()}"
			//log.debug "Summary: ${issue.getSummary()}, Assignee: ${issue.getAssignee().getDisplayName()}, Updated: ${issue.getUpdated()}"

			subject = "TradeFXL Jira review request: ["+issue.getKey()+"] "+issue.getSummary()
			log.debug "subject: " + subject
			body = "Dear "+issue.getReporterUser().getDisplayName()+", \nPlease review the Issue https://jira.internal-services.com/servicedesk/customer/portal/1/"+issue.getKey()+" for\ninformation we have requested and use the\nComment button to provide us with the\ninformation we have requested.\n\nIf the issue has already been resolved,\nplease feel free to simply close this ticket.\n\nThank you for contacting SL2 support team\n\nIssue Assignee: "+issue.getAssignee().getDisplayName()+" ("+issue.getAssignee().getEmailAddress()+")\nLast Update: "+issue.getUpdated().toString()[0..-8]
			log.debug "Body: " + body

         // Test mail
			//sendmail(body , subject, toAddress, fromAddress, host, port)
         // Prod mail
         log.debug "Mail to:" + issue.getReporterUser().getEmailAddress()
         sendmail(body , subject, issue.getReporterUser().getEmailAddress(), fromAddress, host, port)
		}
	} else {
	log.debug("Invalid JQL: " + it)
	}
}
// */

subject = "Escalation: Jira Waiting For Support review"
log.debug "subject: " + subject
body = ""
jqlSL.each {
   SearchService.ParseResult parseResult = searchService.parseQuery(user, it)
   if (parseResult.isValid()) {
      def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
      issues = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
      for ( issue in issues ){
         body=body+"\n["+issue.getKey()+"] Priority: "+issue.getPriorityObject().getName()+" \nSummary:"+issue.getSummary()+"\nPlease review the Issue https://jira.internal-services.com/browse/"+issue.getKey()+"\nAnd provide information to the Reporter.\nIssue Reporter:"+issue.getReporterUser().getDisplayName()+" ("+issue.getReporterUser().getEmailAddress()+")\nLast Update: "+issue.getUpdated().toString()[0..-8]+"\n\n"
      }
   } else {
   log.debug("Invalid JQL: " + it)
   }
}
log.debug "Body: " + body
// Test mail
sendmail(body , subject, toAddress, fromAddress, host, port)
// Prod mail
sendmail(body , subject, slAddress, fromAddress, host, port)