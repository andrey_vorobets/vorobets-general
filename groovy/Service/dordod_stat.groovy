// JQL query like
// (DoR is not EMPTY OR "Definition Comment" is not EMPTY) AND project in (Marketing, "Marketing Java") and updatedDate >= startOfMonth() and updatedDate<= endOfMonth() ORDER BY project, key
/*
class: com.onresolve.jira.groovy.GroovyService
Path in dev:  /opt/atlassian/jira.home.6412/scripts
Path in Prod: /opt/jira-test-home/scripts
*/

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.issue.MutableIssue

import org.apache.log4j.Category

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

import javax.mail.*
import javax.mail.internet.*
import javax.activation.*

Category log = Category.getInstance("com.onresolve.jira.groovy")
log.setLevel(org.apache.log4j.Level.DEBUG)

log.debug "DoR and DoD Statistics Service Debug"

jqlSL = ['(DoR is not EMPTY OR "Definition Comment" is not EMPTY) AND project in (Marketing, "Marketing Java") and updatedDate >= "2016-04-01" and updatedDate<= endOfMonth() ORDER BY project, key']

def String adminUserName = "avorobets"
def toAddress = "vorobets@gmail.com"
def slAddress = "xxx@xxx.com"
def fromAddress = "jira@xxx.com"
def host = "localhost"
def port = "25"

String StoryPoints = "customfield_10002"
String DoR = "customfield_11002"
String DoRDef = "customfield_11003"
String DoD = "customfield_11001"
String DoDDef = "customfield_11004"

SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
UserUtil userUtil = ComponentAccessor.getUserUtil()
User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = componentManager.getIssueManager()
delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default")
Connection conn = ConnectionFactory.getConnection(helperName)


CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField spfield= customFieldManager.getCustomFieldObject(StoryPoints)
CustomField dorfield= customFieldManager.getCustomFieldObject(DoR)
CustomField dordeffield= customFieldManager.getCustomFieldObject(DoRDef)
CustomField dodfield= customFieldManager.getCustomFieldObject(DoD)
CustomField doddeffield= customFieldManager.getCustomFieldObject(DoDDef)

if (!user) {
user = userUtil.getUserObject(adminUserName)
}

List<Issue> issues = null
String subject = "DoR and DoD Statistics, April and May 2016"
log.debug "subject: " + subject

String cssclass="<style type=\"text/css\">.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}.tftable tr {background-color:#d4e3e5;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}.tftable tr:hover {background-color:#ffffff;}</style>"
//String body = cssclass+"<table class=\"tftable\" border='1'><tr><td>Type</td><td>Key</td><td>Priority</td><td>Summary</td><td>Assignee</td><td>Reporter</td><td>Status</td><td>Created</td><td>Updated</td><td>SP</td></tr>"
String body = cssclass+"<table class=\"tftable\" border='1'><tr><td>Type</td><td>Key</td><td>Priority</td><td>Summary</td><td>Assignee</td><td>Reporter</td><td>Status</td><td>Created</td><td>Updated</td><td>SP</td><td>DoR</td><td>DoR Definition</td><td>DoD</td><td>DoD Definition</td></tr>"
def dorMap = []
String dorHTML = ""
String dordefvalue = ""
String dodHTML = ""
String doddefvalue = ""
String dorTable=""
String iassignee = ""
String ireporter = ""

def sendmail(String message ,String subject, String toAddress, String fromAddress, String host, String port){
   Properties mprops = new Properties();
   mprops.setProperty("mail.transport.protocol","smtp");
   mprops.setProperty("mail.host",host);
   mprops.setProperty("mail.smtp.port",port);
 
   Session lSession = Session.getDefaultInstance(mprops,null);
   MimeMessage msg = new MimeMessage(lSession);
 
   StringTokenizer tok = new StringTokenizer(toAddress,";");
   ArrayList emailTos = new ArrayList();
   while(tok.hasMoreElements()){
      emailTos.add(new InternetAddress(tok.nextElement().toString()));
   }
   InternetAddress[] to = new InternetAddress[emailTos.size()];
   to = (InternetAddress[]) emailTos.toArray(to);
   msg.setRecipients(MimeMessage.RecipientType.TO,to);
   msg.setFrom(new InternetAddress(fromAddress));
   msg.setSubject(subject);
   //msg.setText(message)
   msg.setContent(message, "text/html; charset=utf-8")
 
   Transport transporter = lSession.getTransport("smtp");
   transporter.connect();
   transporter.send(msg);
}

//MutableIssue issue = issueManager.getIssueObject("MK-935")
jqlSL.each {
	SearchService.ParseResult parseResult = searchService.parseQuery(user, it)
	if (parseResult.isValid()) {
		def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
		issues = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
		for ( issue in issues ){
			dorMap = issue.getCustomFieldValue(dorfield)
			dorHTML = ""
			dordefvalue = ""
			dordefvalue = issue.getCustomFieldValue(dordeffield)
			dorMap.eachWithIndex { it2, i2 ->
				dorHTML = dorHTML + it2 + "&nbsp;<b>+</b>" + "<br>"
			}
			dodMap = issue.getCustomFieldValue(dodfield)
			dodHTML = ""
			doddefvalue = ""
			doddefvalue = issue.getCustomFieldValue(doddeffield)
			dodMap.eachWithIndex { it2, i2 ->
				dodHTML = dodHTML + it2 + "&nbsp;<b>+</b>" + "<br>"
			}
			//dorTable="<table class=\"tftable\" border='1'><tr><td valign='top' align='left'><b>DoR</b><br>"+dorHTML+"</td><td valign='top' align='left'><b>Definition</b><br>"+dordefvalue+"</td><td valign='top' align='left'><b>DoD</b><br>"+dodHTML+"</td><td valign='top' align='left'><b>Definition</b><br>"+doddefvalue+"</td></tr></table>"
			dorTable="<td valign='top' align='left'>"+dorHTML+"</td><td valign='top' align='left'>"+dordefvalue+"</td><td valign='top' align='left'>"+dodHTML+"</td><td valign='top' align='left'>"+doddefvalue+"</td>"
			try {
				iassignee = issue.getAssigneeUser().getDisplayName()
			} catch (all) {
				iassignee = ""
			}
			try {
				ireporter = issue.getReporterUser().getDisplayName()
			} catch (all) {
				ireporter = ""
			}
			//body=body+"<tr><td>"+issue.getIssueTypeObject().getName()+"</td><td><a href=\"https://jira.internal-services.com/browse/"+issue.getKey()+"\">"+issue.getKey()+"</a></td><td>"+issue.getPriorityObject().getName()+"</td><td>"+issue.getSummary()+"</td><td>"+iassignee+"</td><td>"+ireporter+"</td><td>"+issue.getStatusObject().getName()+"</td><td>"+issue.getCreated().toString()[0..15]+"</td><td>"+issue.getUpdated().toString()[0..15]+"</td><td>"+issue.getCustomFieldValue(spfield)+"</td>"+"</tr>"
			//body=body+"<tr><td colspan=10>"+dorTable+"</td>"+"</tr>"
			body=body+"<tr><td>"+issue.getIssueTypeObject().getName()+"</td><td><a href=\"https://jira.internal-services.com/browse/"+issue.getKey()+"\">"+issue.getKey()+"</a></td><td>"+issue.getPriorityObject().getName()+"</td><td>"+issue.getSummary()+"</td><td>"+iassignee+"</td><td>"+ireporter+"</td><td>"+issue.getStatusObject().getName()+"</td><td>"+issue.getCreated().toString()[0..15]+"</td><td>"+issue.getUpdated().toString()[0..15]+"</td><td>"+issue.getCustomFieldValue(spfield)+"</td>"+dorTable+"</tr>"
		}
	} else {
   		log.debug("Invalid JQL: " + it)
    }
}

body=body+"</table></form>"
log.debug("Body " + body)
sendmail(body , subject, slAddress, fromAddress, host, port)
sendmail(body , subject, toAddress, fromAddress, host, port)