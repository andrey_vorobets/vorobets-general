import com.atlassian.jira.issue.Issue
import com.opensymphony.workflow.InvalidInputException
import org.apache.log4j.Category

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")
 
Issue myIssue = issue
String HrRequest = "Access"
String UserIdentifier = "customfield_11104"

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField uifield= customFieldManager.getCustomFieldObject(UserIdentifier)

log.debug "ITENT Validator - UserIdentifier field: " + myIssue.getCustomFieldValue(uifield)
log.debug "ITENT Validator - Issue Type: " + myIssue.getIssueTypeObject().getName()

// User UserIdentifier field: null

if (myIssue.getIssueTypeObject().getName()==HrRequest && myIssue.getCustomFieldValue(uifield).toString()=="null") {
	InvalidInputException error= new InvalidInputException();
	error.addError("UserIdentifier field must be filled to resolve this issueType");
	throw error;
}