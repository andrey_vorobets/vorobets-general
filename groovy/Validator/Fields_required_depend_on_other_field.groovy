import com.atlassian.jira.issue.Issue
import com.opensymphony.workflow.InvalidInputException
import org.apache.log4j.Category

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

//import com.atlassian.jira.issue.link.IssueLink

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
 
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

String checkingIssueType = "Problem"
String impactValue = "Full functionality loss"
String impactClient = "customfield_10705"
String startTime = "customfield_10703"
String endTime = "customfield_10704"

Issue myIssue = issue
//IssueManager issueManager = ComponentAccessor.getIssueManager()
//MutableIssue myIssue = issueManager.getIssueObject("ITSERV-1909")

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()

CustomField impactClientfield= customFieldManager.getCustomFieldObject(impactClient)
CustomField startTimefield= customFieldManager.getCustomFieldObject(startTime)
CustomField endTimefield= customFieldManager.getCustomFieldObject(endTime)

//log.debug "Validators - fields start: " + myIssue.getCustomFieldValue(startTimefield)
//log.debug "Validators - fields end: " + myIssue.getCustomFieldValue(endTimefield)

/*if (myIssue.getCustomFieldValue(startTimefield)==null){
	log.debug "Validators - fields start: " + myIssue.getCustomFieldValue(startTimefield)
}*/
if (myIssue.getIssueTypeObject().getName()==checkingIssueType) {
	if (myIssue.getCustomFieldValue(impactClientfield).toString()==impactValue) {
		log.debug "Validators - Impact for client field: " + myIssue.getCustomFieldValue(impactClientfield)
		if (myIssue.getCustomFieldValue(startTimefield)==null || myIssue.getCustomFieldValue(endTimefield)==null) {
			InvalidInputException error= new InvalidInputException();
			error.addError("Full functionality loss: Start and End Time fields is required");
			throw error;
		}
	}
}