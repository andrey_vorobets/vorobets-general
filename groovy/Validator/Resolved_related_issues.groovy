import com.atlassian.jira.issue.Issue
import com.opensymphony.workflow.InvalidInputException
import org.apache.log4j.Category

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.link.IssueLink

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager

String linkname="Encorporation"

def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

Issue myIssue = issue

//IssueManager issueManager = ComponentAccessor.getIssueManager()
//MutableIssue myIssue = issueManager.getIssueObject("CFD-3249")

ComponentManager componentManager = ComponentManager.getInstance()
issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()

log.debug("Resolved related issues: " + myIssue.key)
/*
for (IssueLink link in issueLinkManager.getOutwardLinks(issue.getId())){
	log.debug("Links by name debug : " + link.getIssueLinkType().getName())
	log.debug("Links by ID : " + link.getSourceObject().getKey())
}*/
for (IssueLink link in issueLinkManager.getOutwardLinks(myIssue.getId())){
	//log.debug("linked issue: " + link.getDestinationObject().getKey())
	if (link.getIssueLinkType().getName()==linkname && link.getDestinationObject().getKey()!=myIssue.getKey()){
		log.debug("Resolved related issues links: " + link.getIssueLinkType().getName())
		log.debug("Resolved related issues link id: " + link.getDestinationObject().key)
		log.debug("Linked issue Category :" + link.getDestinationObject().getStatusObject().getStatusCategory().getName())
		
		if (link.getDestinationObject().getStatusObject().getStatusCategory().getName()!="Complete") {
			InvalidInputException error= new InvalidInputException();
			error.addError("All Encorporated Issues must be resolved");
			throw error;
			break;
		}
		
	}
}