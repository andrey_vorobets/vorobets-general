import com.atlassian.jira.component.ComponentAccessor
def groupManager = ComponentAccessor.getGroupManager()
groupManager.isUserInGroup(issue.reporter?.name, 'jira-sys-admins')