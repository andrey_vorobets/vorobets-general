import com.atlassian.jira.issue.Issue
import com.opensymphony.workflow.InvalidInputException
import org.apache.log4j.Category

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.link.IssueLink

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager

def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

String first_issuetype="Problem"
String second_issuetype="Known Issue"
Integer founds = 0

Issue myIssue = issue
//IssueManager issueManager = ComponentAccessor.getIssueManager()
//MutableIssue myIssue = issueManager.getIssueObject("ITSERV-2946")

ComponentManager componentManager = ComponentManager.getInstance()
issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()

log.debug("Has linked issuetype: " + myIssue.key)

for (IssueLink link in issueLinkManager.getOutwardLinks(myIssue.getId())){
	if (link.getDestinationObject().getIssueTypeObject().getName()==first_issuetype || link.getDestinationObject().getIssueTypeObject().getName()==second_issuetype) {
		log.debug("Has linked issuetype links: " + link.getDestinationObject().getIssueTypeObject().getName())
		founds = founds + 1
	}
}
if (founds==0) {
	//log.debug("Has linked issuetype links: " + link.getDestinationObject().getIssueTypeObject().getName())
	InvalidInputException error= new InvalidInputException();
	error.addError("Incident must be linked to the Problem or Known Issue");
	throw error;
}