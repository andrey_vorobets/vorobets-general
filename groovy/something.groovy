// work with UserUtils - find user by e-mail

import com.atlassian.jira.user.UserUtils
import com.atlassian.crowd.embedded.api.User

        if (j.get("ClientEmail").size()>1) {
          User validUser = com.atlassian.jira.user.UserUtils.getUserByEmail(j.get("ClientEmail"))
            if (validUser!=null) {
                println "Valid User:" + validUser.getDisplayName()    
            } else {
                println "No user found for e-mail: " + j.get("ClientEmail")
            }
        } else {
            println "No email: " + j.get("ClientName")
        }

/* ---------------------------------------------------------------------------------------------------- */
// csv parser

/@Grab('org.apache.commons:commons-csv:1.2')
// CSV as TAB delimiter
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVFormat
import static org.apache.commons.csv.CSVFormat.*

import java.nio.file.Paths
import groovy.json.JsonOutput
Paths.get('/jira/tmp/MIGRATION_DATA2.csv').withReader('ISO-8859-1') { reader ->
    //CSVParser csv = new CSVParser(reader, DEFAULT.withHeader())
    // Use TAB delimeter
    CSVParser csv = new CSVParser(reader, CSVFormat.TDF.withHeader().withQuote(null))
    
    /*for (record in csv.iterator()) {
        //println record.dump()
        //println (record.toMap() as Expando)
        println record
    }*/
    /*for (record in csv.iterator()) {
        listing << (record.toMap() as Expando)
    }*/
    //println csv.getRecords()
    // https://commons.apache.org/proper/commons-csv/apidocs/org/apache/commons/csv/CSVRecord.html
    for (j in csv.getRecords()) {
          //println (j.toMap() as Expando)
          j.toMap() as Expando
//        println j.get(2)
          //println j.get("ClientEmail")

    }
}

// Create issue
import com.atlassian.jira.user.util.UserUtil
// import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
// import com.atlassian.jira.issue.link.IssueLink
// import com.atlassian.jira.util.ImportUtils
import com.atlassian.crowd.embedded.api.User
// import com.opensymphony.workflow.WorkflowContext
// import org.apache.log4j.Category
import com.atlassian.jira.project.Project
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.project.ProjectManager
//import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.IssueFactory
import com.atlassian.jira.issue.Issue
//import org.ofbiz.core.entity.GenericValue

//log = Category.getInstance("com.onresolve.jira.groovy.CreateDependentIssue")
IssueManager issueMgr = ComponentAccessor.getIssueManager()
ProjectManager projectMgr = ComponentAccessor.getProjectManager()
UserUtil userUtil = ComponentAccessor.getUserUtil()

String asUserName = "avorobets"
User user = userUtil.getUserObject(asUserName)
Project curPrj = projectMgr.getProjectObjByKey("SAND")

IssueFactory issueFactory = ComponentAccessor.getIssueFactory()
Issue newissue = issueFactory.getIssue()
newissue.setSummary ("Create Summary")
newissue.setProjectObject (curPrj)
newissue.setIssueTypeId("3")
newissue.description = "issue.description"
newissue.reporter = user
newissue.assignee = user
//Map params = ["issue":newissue]
Issue created = issueMgr.createIssueObject(user, newissue)
println created.getKey()

// String to data or timestamp
import java.text.*
import java.sql.Timestamp
//String timestamp = "2011-10-02-18.48.05.123";
String timestamp = "11/17/2015 7:30:00 AM"
//DateFormat df = new SimpleDateFormat("yyyy-MM-dd-kk.mm.ss.SSS");
DateFormat df = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss aa");
Date parsedDate = df.parse(timestamp);

// Users search
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.user.search.UserPickerSearchService
import com.atlassian.jira.bc.user.search.UserSearchParams
//import com.atlassian.jira.user.ApplicationUser
import com.atlassian.crowd.embedded.api.User
import java.util.List

def searchService = ComponentAccessor.getComponent(UserPickerSearchService)
UserSearchParams userSearchParams = new UserSearchParams(true, true, true)

List<User> listofUsers = searchService.findUsers("Centeno", userSearchParams)
if (listofUsers.size()>=1) {
	User foundUser = listofUsers[0]
	foundUser.getDisplayName()
}


// Advanced User search
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.user.search.UserPickerSearchService
import com.atlassian.jira.bc.user.search.UserSearchParams
import com.atlassian.jira.user.util.UserUtil
//import com.atlassian.jira.user.ApplicationUser
import com.atlassian.crowd.embedded.api.User
import java.util.List

String asUserName = "avorobets"
UserUtil userUtil = ComponentAccessor.getUserUtil()
User user = userUtil.getUserObject(asUserName)

User trytoFindUser(String searchstring, User user) {
    def searchService = ComponentAccessor.getComponent(UserPickerSearchService)
    UserSearchParams userSearchParams = new UserSearchParams(true, true, true)
    String parsedsearch
    String secondPart
    try {
        if (searchstring.tokenize(" ").size()>0) {
            parsedsearch = searchstring.tokenize(".")[1]
            secondPart = searchstring.tokenize(".")[0]
            println parsedsearch + " Second part: " + secondPart
        } else {
            parsedsearch = searchstring
        }
    } catch(all) {
        println "Error in searchstring"
        return user
    }
    List<User> listofUsers = searchService.findUsers(parsedsearch, userSearchParams)
    println listofUsers
    if (listofUsers.size()>=1) {
        listofUsers.each {
            println it.getDisplayName()
            if (it.getDisplayName().tr('A-Z', 'a-z').contains(secondPart.tr('A-Z', 'a-z'))) {
                user=it
            }
        }
        //User foundUser = listofUsers[0]
        //foundUser.getDisplayName()
    }
    return user
}

User foundUSer = trytoFindUser("Kathleen.Byrnes", user)
foundUSer
foundUSer.getName()

// AppUser search

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.user.search.UserPickerSearchService
import com.atlassian.jira.bc.user.search.UserSearchParams
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.crowd.embedded.api.User
import java.util.List

String asUserName = "avorobets"
UserUtil userUtil = ComponentAccessor.getUserUtil()
ApplicationUser appuser = ComponentAccessor.userManager.getUserByKey(asUserName)

ApplicationUser trytoFindAppUser(String searchstring, ApplicationUser user) {
	//def Category log = Category.getInstance("com.onresolve.jira.groovy.csv.read.ESA")
    def searchService = ComponentAccessor.getComponent(UserPickerSearchService)
    UserSearchParams userSearchParams = new UserSearchParams(true, true, true)
    String parsedsearch
    String secondPart

    try {
        if (searchstring.tokenize(" ").size()>0) {
            parsedsearch = searchstring.tokenize(" ")[1]
            secondPart = searchstring.tokenize(" ")[0]
        } else {
            parsedsearch = searchstring
        }
        //log.debug "parsedsearch: " + parsedsearch
    } catch(all) {
        //log.error ("Error in searchstring")
        return user
    }
    List<User> listofUsers = searchService.findUsers(parsedsearch, userSearchParams)
    if (listofUsers.size()>=1) {
    	listofUsers.each {
    		if (it.getDisplayName().tr('A-Z', 'a-z').contains(secondPart.tr('A-Z', 'a-z'))) {
                user = ComponentAccessor.userManager.getUserByKey(it.getName())
            }
    	}
    }

	return user
}
ApplicationUser foundUSer = trytoFindAppUser("Kathleen Byrnes", appuser)
foundUSer



// Duration between two days
import java.sql.Timestamp
import java.text.*
import groovy.time.TimeCategory
String date2="27/Apr/16"
String date1="15/Jul/16"

Timestamp getTimestamp(String inputdate) {
    DateFormat df = new SimpleDateFormat("dd/MMM/yy")
    Date parsedDate = df.parse(inputdate)
    Timestamp ts = new Timestamp(parsedDate.getTime())
    return ts
}

    def duration = getTimestamp(date1) - getTimestamp(date2)
println getTimestamp(date1)
//    println "Days: ${duration.days}, Hours: ${duration.hours}, etc."
println "Duration: " + duration

// Groovy console log - test non-break on long run
import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")
Integer i
for (i = 0; i <600; i++) {
	log.warn ("Processed issues $i")
	sleep(5000)
}

// Attachments
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.security.JiraAuthenticationContext

import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean
import com.atlassian.jira.issue.AttachmentManager
import java.nio.file.Path
import java.nio.file.Paths
import java.io.File
import java.nio.file.Files

ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = ComponentAccessor.getIssueManager()
AttachmentManager attachmentManager = ComponentAccessor.getAttachmentManager()
UserUtil userUtil = ComponentAccessor.getUserUtil()

String adminUserName = "avorobets"
ApplicationUser admuser = ComponentAccessor.getJiraAuthenticationContext().getUser()
if (!admuser) {
admuser = userUtil.getUserByName(adminUserName)
}

MutableIssue issue = issueManager.getIssueObject("SAND-8")
println issue.id

//File tmpFile = new File("/jira/tmp/SMR/SMR-000441/Change of Address and W9 -  David Glass.pdf")
Path source = Paths.get("/jira/tmp/SMR/SMR-000441/Change of Abla bla.pdf")
String ctype = Files.probeContentType(source)
//CreateAttachmentParamsBean bean = new CreateAttachmentParamsBean.Builder(tmpFile, tmpFile.getName(), "application/octet-stream", reporterApp, issue).build()
CreateAttachmentParamsBean bean = new CreateAttachmentParamsBean.Builder(source.toFile(), source.toFile().getName(), ctype, admuser, issue).build()
//attachmentManager.createAttachment(bean)

// Drop all comments
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.comments.Comment
import com.atlassian.jira.issue.comments.CommentManager
 
String issueKey = 'SMRRO-474'
 
IssueManager issueManager = ComponentAccessor.issueManager
CommentManager commentManager = ComponentAccessor.commentManager
 
MutableIssue issue = issueManager.getIssueObject(issueKey)
List<Comment> comments = commentManager.getComments(issue)
comments.each {comment ->
    //if (comment.body.contains('Bla Bla Bla')) {
        commentManager.delete(comment)
    //}
}

// Simple Logger
import java.text.*
class Logger {
    private File logFile = new File("/jira/tmp/csv_comments.log")
    private String dateFormat = "dd.MM.yyyy;HH:mm:ss.SSS"
    private boolean printToConsole = false
 
    /**
     * Catch all defined logging levels, throw  MissingMethodException otherwise
     * @param name
     * @param args
     * @return
     */
    def methodMissing(String name, args) {
        def messsage = args[0]
        if (printToConsole) {
            println messsage
        }
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat)
        String date = formatter.format(new Date())
        switch (name.toLowerCase()) {
            case "trace":
                logFile << "${date} TRACE ${messsage}\n"
                break
            case "debug":
                logFile << "${date} DEBUG ${messsage}\n"
                break
            case "info":
                logFile << "${date} INFO  ${messsage}\n"
                break
            case "warn":
                logFile << "${date} WARN  ${messsage}\n"
                break
            case "error":
                logFile << "${date} ERROR ${messsage}\n"
                break
            default:
                throw new MissingMethodException(name, delegate, args)
        }
    }
}

def logger = new Logger()
logger.trace "Trace level test"
logger.debug "Debug level test"
logger.info "Info level test"
logger.warn "Warn level test"
logger.error "Error level test"

// Last comment date
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.comments.Comment
import java.util.Date

IssueManager issueManager = ComponentAccessor.getIssueManager()
Date commentDate
MutableIssue issue = issueManager.getIssueObject("SAND-1")
List<Comment> comments = ComponentAccessor.commentManager.getComments(issue)
if (!comments) {
    return null
}
Comment last = comments.last()
Date commentCreated = last.getCreated()
Date commentUpdated = last.getUpdated()
if (commentUpdated>commentCreated){
	commentDate = commentUpdated
} else {
	commentDate = commentCreated
}
commentDate


// Transition, base
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.user.UserUtils
import com.atlassian.jira.user.util.UserUtil

import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.issue.IssueInputParametersImpl

import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.csv.read.ESA")

ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = ComponentAccessor.getIssueManager()
IssueService issueService = ComponentAccessor.getIssueService()

UserUtil userUtil = ComponentAccessor.getUserUtil()
String asUserName = "dmoro"
User user = userUtil.getUserObject(asUserName)

//def cwdUser = ComponentAccessor.jiraAuthenticationContext.getUser()//CRowd User

//String DoRWarning = "customfield_11006"
//CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
//CustomField cf= customFieldManager.getCustomFieldObject(DoRWarning) 

MutableIssue issue = issueManager.getIssueObject("ESA-2964")

def issueInputParameters = new IssueInputParametersImpl()
def constantManager = ComponentAccessor.getConstantsManager()

String comment = "Testing auto transition"
def doneStatusId = constantManager.getStatusObjects().find {it.name == "Internal Review"}?.id
//def doneResolutionId = constantManager.getResolutionObjects().find {it.name == "Done"}?.id
//issueInputParameters.setResolutionId(doneResolutionId).setStatusId(doneStatusId).setComment(comment)
issueInputParameters.setStatusId(doneStatusId).setComment(comment)
def transitionValidationResult = issueService.validateTransition(user, issue.id, 421, issueInputParameters)
// Number - transition id REPORT RECEIVED & PREP REBUTTAL - 421
// INTERNAL REVIEW - 421
log.error("Validation result : " + transitionValidationResult.errorCollection)
if (transitionValidationResult.isValid()) {
	//log.debug("Transition for sub task ${it.key} is valid")
	issueService.transition(user, transitionValidationResult)
}

issue.id

/* ---------------------------------------------------------------------------------------------------- */
// Mutable issue in Jira 7
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.IssueManager
…
def issueManager = ComponentAccessor.getIssueManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def mycf = customFieldManager.getCustomFieldObject("customfield_12345")
…
def mutableIssue = issueManager.getIssueObject(issue.id)
mutableIssue.setCustomFieldValue (mycf, value)
issueManager.updateIssue(user, mutableIssue, EventDispatchOption.ISSUE_UPDATED, false)