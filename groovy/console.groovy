import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = ComponentAccessor.getIssueManager()
String DoRWarning = "customfield_11700"
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject(DoRWarning) 

MutableIssue issue = issueManager.getIssueObject("SAND-48")
Options options = cf.getOptions(null, cf.getRelevantConfig(issue), null)

delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");
Connection conn = ConnectionFactory.getConnection(helperName);

Sql sql = new Sql(conn)
def sqlStmt = "select * from dor_status('"+issue.id+"')"
String dor_res = ""
sql.eachRow(sqlStmt) {row ->
dor_res = row.dor.toString()
}
sql.close()
dor_res
cf.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(cf), dor_res),changeHolder)

// Groovy resource
import javax.naming.InitialContext
import groovy.sql.Sql
import javax.naming.Context
import javax.sql.DataSource

import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

log.error "Something"
String dor_res = ""
try {
    // Get Context to connect to database as a datasource
    Context ctx = new InitialContext()
    if (ctx == null) {
        log.error "No DS"
        throw new Exception("No Context found!")
    }
    DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/Crowd")
    log.error ds
    if (ds) {
        def sql = new Sql(ds)
        def sqlStmt = "select user_name from cwd_user limit 5"
        
        sql.eachRow(sqlStmt) {row ->
        	log.eror row
			dor_res = row.toString()
		}
		sql.close()
    }
} catch (all) {
    log.error "Error" + all
}
dor_res
// minimal

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = ComponentAccessor.getIssueManager()

String DoRWarning = "customfield_11006"
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject(DoRWarning) 

MutableIssue issue = issueManager.getIssueObject("SAND-20")
issue.id
