/*
JQL: project=INCPORT and priority=Critical and issueFunction in lastComment("before -1d") and status = "Waiting for customer"
USER ID: mzaiats
Action: None

*/

issueInputParameters.setComment('Dear Reporter, \nPlease review the Issue for\n information we have requested and use the\n Comment button to provide us with the\n information we have requested.\n\n If the issue has already been resolved,\n please feel free to simply close this ticket.\n\n Thank you for contacting SL2 support team')
