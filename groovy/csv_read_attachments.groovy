import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVFormat
import static org.apache.commons.csv.CSVFormat.*

import java.nio.file.Paths
import java.nio.file.Path
import java.io.File
import java.nio.file.Files

import groovy.json.JsonOutput
import java.text.*
import java.sql.Timestamp
import java.util.Collection
import java.util.List

import com.atlassian.jira.user.UserUtils
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.bc.user.search.UserPickerSearchService
import com.atlassian.jira.bc.user.search.UserSearchParams
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.project.Project
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.issue.IssueFactory
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.web.bean.PagerFilter

import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean
import com.atlassian.jira.issue.AttachmentManager

import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

IssueManager issueMgr = ComponentAccessor.getIssueManager()
ProjectManager projectMgr = ComponentAccessor.getProjectManager()
UserUtil userUtil = ComponentAccessor.getUserUtil()
IssueFactory issueFactory = ComponentAccessor.getIssueFactory()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
AttachmentManager attachmentManager = ComponentAccessor.getAttachmentManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()

String asUserName = "smrclient"
String adminUserName = "avorobets"
User user = userUtil.getUserObject(asUserName)
Project curPrj = projectMgr.getProjectObjByKey("SMRRO")
Integer a = 0
Integer i
Integer checkAttachments
String attName
Path source
String ctype
def listing = [:]
MutableIssue issue
String fileExt
// http://www.freeformatter.com/mime-types-list.html
def cTypes = ['.3gp':'video/3gpp', '.7z':'application/x-7z-compressed', '.swf':'application/x-shockwave-flash', '.pdf':'application/pdf', '.avi':'video/x-msvideo', '.bmp':'image/bmp', '.csv':'text/csv', '.djvu':'image/vnd.djvu', '.dwg':'image/vnd.dwg', '.epub':'application/epub+zip', '.eml':'message/rfc822', '.flv':'video/x-flv', '.gif':'image/gif', '.java':'text/x-java-source,java', '.js':'application/javascript', '.exe':'application/x-msdownload', '.xls':'application/vnd.ms-excel', '.chm':'application/vnd.ms-htmlhelp', '.pptx':'application/vnd.openxmlformats-officedocument.presentationml.presentation', '.xlsx':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '.docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '.ppt':'application/vnd.ms-powerpoint', '.vsd':'application/vnd.visio', '.vsdx':'application/vnd.visio2013', '.doc':'application/msword', '.psd':'image/vnd.adobe.photoshop', '.png':'image/png', '.rar':'application/x-rar-compressed', '.rtf':'application/rtf', '.svg':'image/svg+xml', '.tiff':'image/tiff', '.xml':'application/xml', '.zip':'application/zip', '.jpeg':'image/jpeg', '.jpg':'image/jpeg', '.msg':'application/vnd.ms-outlook']

String originalID = "customfield_14779" // Original ID
CustomField originalIDObj= customFieldManager.getCustomFieldObject(originalID)

User admuser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
if (!admuser) {
	admuser = userUtil.getUserObject(adminUserName)
}

//ApplicationUser appuser = ComponentAccessor.getJiraAuthenticationContext().getUser()
//if (!appuser) {
ApplicationUser appuser = userUtil.getUserByName(asUserName)
//}

class Logger {
    private File logFile = new File("/jira/tmp/csv_attachments.log")
    private File logFileErr = new File("/jira/tmp/csv_attachments_error.log")
    private String dateFormat = "dd.MM.yyyy;HH:mm:ss.SSS"
    private boolean printToConsole = false
 
    /**
     * Catch all defined logging levels, throw  MissingMethodException otherwise
     * @param name
     * @param args
     * @return
     */
    def methodMissing(String name, args) {
        def messsage = args[0]
        if (printToConsole) {
            println messsage
        }
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat)
        String date = formatter.format(new Date())
        switch (name.toLowerCase()) {
            case "trace":
                logFile << "${date} TRACE ${messsage}\n"
                break
            case "debug":
                logFile << "${date} DEBUG ${messsage}\n"
                break
            case "info":
                logFile << "${date} INFO  ${messsage}\n"
                break
            case "warn":
                logFile << "${date} WARN  ${messsage}\n"
                break
            case "error":
                logFile << "${date} ERROR ${messsage}\n"
                logFileErr << "${date} ERROR ${messsage}\n"
                break
            default:
                throw new MissingMethodException(name, delegate, args)
        }
    }
}
def logger = new Logger()

String jQL = "project = SMRRO"
List<Issue> issueList
SearchService.ParseResult parseResult = searchService.parseQuery(admuser, jQL)
if (parseResult.isValid()) {
	SearchResults searchResult = searchService.search(admuser, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
	issueList = searchResult.getIssues()
	//println issueList
	if (issueList.size()>=1) {
		issueList.each {
			//println it.getKey()
			listing[it.getCustomFieldValue(originalIDObj).toString()]=[it.getKey()]
		}
	}
} else {
	log.warn ("Invalid JQL: " + jQL)
	//log.debug("Invalid JQL: " + it)
}
//println listing

//issueList
if (issueList.size()>=1) {
Paths.get('/jira/tmp/AttachmentData6.csv').withReader('UTF-8') { reader ->
	CSVParser csv = new CSVParser(reader, CSVFormat.TDF.withHeader().withQuote(null))
	for (j in csv.getRecords()) {
		j.toMap() as Expando
		logger.trace "Ticker number: " + j.get("TicketNumber")
		for (i = 1; i <541; i++) {
			attName = "Attachment "+i.toString()
			if (j.get(attName).size()>1) {
				logger.trace "Attachment number " + i +": " + j.get(attName).drop(29).replaceAll('\\\\', "/")
				source = Paths.get("/jira/tmp/"+j.get(attName).drop(29).replaceAll('\\\\', "/"))
				logger.trace "File ext: " + source.toFile().getName()[source.toFile().getName().lastIndexOf('.') .. source.toFile().getName().length()-1]
				fileExt = source.toFile().getName()[source.toFile().getName().lastIndexOf('.') .. source.toFile().getName().length()-1]
				if (cTypes[fileExt]!=null){
					ctype = cTypes[fileExt]
				} else {
					ctype = "application/octet-stream"
				}
				//ctype = Files.probeContentType(source)
				logger.trace "File Type: " + ctype
				if (ctype==null) {
					ctype="application/octet-stream"
				}
				//println "Issue: " + listing[j.get("TicketNumber")][0].getClass()
				logger.trace "Issue: " + listing[j.get("TicketNumber")][0]
				issue = issueManager.getIssueObject(listing[j.get("TicketNumber")][0])
				//println issue.getReporter()
				checkAttachments = 0
				issue.getAttachments().each {
					if (it.getFilename()==source.toFile().getName()) {
						checkAttachments = 1
					}
				}
				if (checkAttachments == 0) {
					CreateAttachmentParamsBean bean = new CreateAttachmentParamsBean.Builder(source.toFile(), source.toFile().getName(), ctype, appuser, issue).build()
					try {
						attachmentManager.createAttachment(bean)
						logger.warn "CSV Import  - File added: " + source.toFile().getName()
					} catch (all) {
						logger.error "CSV import attachment - Bad file: " + source.toFile().getName() + " FUll path: " + source
						logger.error all
					}
				}
			}
		}
		log.warn ("Processed issues $a")
		a++
	}
}
}