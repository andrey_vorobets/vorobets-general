import com.onresolve.jira.groovy.user.FieldBehaviours
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Category
import static com.atlassian.jira.issue.IssueFieldConstants.PRIORITY

class ESRBehaviourBusinessUnitInit extends FieldBehaviours {
public Object run() {

    def constantsManager = ComponentAccessor.getConstantsManager()
    def allowedPriorities = constantsManager.getPriorityObjects().findAll {
        if (["Blocker","Critical","Major","Minor","On Hold"].contains(it.name)) {
            true
        } else {
            false
        }
    }.collectEntries {
        [(it.id): it.name]
    }
    if (getActionName() == "Create Issue") {
        getFieldById(PRIORITY).setFormValue("Major")
    }
    getFieldById(PRIORITY).setFieldOptions(allowedPriorities)

}
}

