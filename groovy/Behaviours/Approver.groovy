import com.atlassian.jira.bc.project.component.ProjectComponent
FormField componentFF = getFieldById(fieldChanged)
FormField userPickerFF = getFieldByName("Approver")
List<ProjectComponent> components = componentFF.getValue() as List
if(components){
    userPickerFF.setFormValue(components.first().lead)
} else {
    userPickerFF.setFormValue("")
}