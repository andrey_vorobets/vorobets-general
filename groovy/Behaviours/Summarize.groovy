import com.atlassian.jira.issue.comments.CommentManager
import com.atlassian.jira.issue.comments.Comment
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager

CommentManager commentMgr = ComponentAccessor.getCommentManager();
def projSum = getFieldByName("Summarize")
String concat = ""

Collection<Comment> comments = commentMgr.getComments(underlyingIssue);
for (Comment comment : comments) {
	concat = concat + "[~"+comment.getAuthor()+"] "+comment.getAuthor()+" "+comment.getCreated()+"\n"+"---- \n"+comment.getBody() + "\n\n";
}
projSum.setFormValue(concat)