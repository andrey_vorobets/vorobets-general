// Default description

def desc = getFieldById("description")

def defaultValue = """*The story:*
When you create a tag you may accidentally click outside the form,you don't want your work to be lost after that.
Impact: absent

*Steps:*
- Open new tag form
- Click outside the form

*Expected:*
 - Nothing happens

*Actual:*
- Form closed and if reopened,nothing is saved.""".replaceAll(/    /, '')

if (! underlyingIssue?.description) { 
    desc.setFormValue(defaultValue)
}