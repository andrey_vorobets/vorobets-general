import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

def optionsManager = ComponentAccessor.getOptionsManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
String crmbo = "CRMBO"

def proPlatFld = getFieldByName("Production Platform")
def prodServFld = getFieldByName("Production Services")
def impactFld = getFieldByName("Impact")

def selectedVal = proPlatFld.getValue()
log.debug("Behaviour: " + selectedVal.toString())
log.debug("Behaviour 2: " + selectedVal)
if (selectedVal[0].toString() == "CRMBO") {
	log.debug("Behaviour: got CRMBO")
	customField = customFieldManager.getCustomFieldObject(prodServFld.getFieldId())
	config = customField.getRelevantConfig(getIssueContext())
	options = optionsManager.getOptions(config)
	def optionsToSelect = options.findAll { it.value in ["Widget Login", "Widget Deposit", "TISS/BPSS/FTSS", "RPA", "SPA", "Platon", "SAS"] } 
	prodServFld.setFormValue(optionsToSelect*.optionId)

	customField2 = customFieldManager.getCustomFieldObject(impactFld.getFieldId())
	config2 = customField2.getRelevantConfig(getIssueContext())
	options2 = optionsManager.getOptions(config2)
	def optionsToSelect2 = options2.findAll { it.value in ["Client can not login", "Client can not register", "Client can not trade", "Client can not deposit", "Problems with equity", "Problems with bonuses", "Problems with reports"] } 
	impactFld.setFormValue(optionsToSelect2*.optionId)

	customField3 = customFieldManager.getCustomFieldObject(proPlatFld.getFieldId())
	config3 = customField3.getRelevantConfig(getIssueContext())
	options3 = optionsManager.getOptions(config3)
	def optionsToSelect3 = options3.findAll { it.value in ["CRMBO", "Cosmos_web_live", "Cosmos_web_demo", "Cosmos_mob_live", "Cosmos_mob_demo", "MT4 USD", "MT4 EURO", "MT4 Finexo", "MT4 Demo", "MT4 LC – Live", "MT4 LC – Demo", "MT4 HK – Live", "MT4 HK – Demo", "MT4 A1", "Webtrader-1", "Webtrader-2", "Webtrader-3"] } 
	proPlatFld.setFormValue(optionsToSelect3*.optionId)
}