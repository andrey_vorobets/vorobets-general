// Change tab by \t to 4spaces in LO (Other Options / Regular expressions)
// Change new line by \n to \\ in LO (Other Options / Regular expressions)
// Change " to \"
// Change CreatedOn format to MM/DD/YYYY HH:MM:SS
// Change [\\]{3,} to space in LO (Other Options / Regular expressions)
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVFormat
import static org.apache.commons.csv.CSVFormat.*

import java.util.List
import java.nio.file.Paths
import java.nio.file.Path
import java.sql.Timestamp
import java.text.*

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.comments.CommentManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.web.bean.PagerFilter

import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

CommentManager commentManager = ComponentAccessor.getCommentManager()
UserUtil userUtil = ComponentAccessor.getUserUtil()
SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()

String adminUserName = "avorobets"
String asUserName = "smrclient"
String commName
Integer a = 0
Integer i
Integer checkComm
def listing = [:]
ApplicationUser appuser = userUtil.getUserByName(asUserName)
MutableIssue issue

String originalID = "customfield_14779" // Original ID
CustomField originalIDObj= customFieldManager.getCustomFieldObject(originalID)

Timestamp getTimestamp(String inputdate) {
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss")
    Date parsedDate = df.parse(inputdate)
    Timestamp ts = new Timestamp(parsedDate.getTime())
    return ts
}

User admuser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
if (!admuser) {
	admuser = userUtil.getUserObject(adminUserName)
}

class Logger {
    private File logFile = new File("/jira/tmp/csv_comments.log")
    private String dateFormat = "dd.MM.yyyy;HH:mm:ss.SSS"
    private boolean printToConsole = false
 
    /**
     * Catch all defined logging levels, throw  MissingMethodException otherwise
     * @param name
     * @param args
     * @return
     */
    def methodMissing(String name, args) {
        def messsage = args[0]
        if (printToConsole) {
            println messsage
        }
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat)
        String date = formatter.format(new Date())
        switch (name.toLowerCase()) {
            case "trace":
                logFile << "${date} TRACE ${messsage}\n"
                break
            case "debug":
                logFile << "${date} DEBUG ${messsage}\n"
                break
            case "info":
                logFile << "${date} INFO  ${messsage}\n"
                break
            case "warn":
                logFile << "${date} WARN  ${messsage}\n"
                break
            case "error":
                logFile << "${date} ERROR ${messsage}\n"
                break
            default:
                throw new MissingMethodException(name, delegate, args)
        }
    }
}
def logger = new Logger()

String jQL = "project = SMRRO"
List<Issue> issueList
SearchService.ParseResult parseResult = searchService.parseQuery(admuser, jQL)
if (parseResult.isValid()) {
	SearchResults searchResult = searchService.search(admuser, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
	issueList = searchResult.getIssues()
	//println issueList
	if (issueList.size()>=1) {
		issueList.each {
			//println it.getKey()
			listing[it.getCustomFieldValue(originalIDObj).toString()]=[it.getKey()]
		}
	}
} else {
	log.warn ("Invalid JQL: " + jQL)
	//log.debug("Invalid JQL: " + it)
}
logger.trace "Issue List size: " + issueList.size()
if (issueList.size()>=1) {
	Paths.get('/jira/tmp/CommentData2.csv').withReader('UTF-8') { reader ->
		CSVParser csv = new CSVParser(reader, CSVFormat.TDF.withHeader().withQuote(null))
		for (j in csv.getRecords()) {
			j.toMap() as Expando
			logger.trace "Ticker number: " + j.get("TicketNumber")
			for (i = 1; i <129; i++) {
				commName = "Comment "+i.toString()
				if (j.get(commName).size()>1) {
					//logger.warn "Try to insert comment: #" + i.toString() + ": " + j.get(commName)
					logger.warn "Try to insert comment: #" + i.toString()
					issue = issueManager.getIssueObject(listing[j.get("TicketNumber")][0])
					logger.debug "csv read comment - Issue: " + issue.getKey() + " TicketNumber: " + j.get("TicketNumber")
					checkComm = 0
					/*commentManager.getComments(issue).each {
						if (it.getBody()==j.get(commName)) {
							checkComm = 1
							println "Comment already present, skipping"
						}
					}*/
					if (checkComm==0) {
						try {
						if (j.get("CreatedOn").size()>1) {
							commentManager.create(issue, appuser, null, j.get(commName), null, null, getTimestamp(j.get("CreatedOn")), null, false)
						} else {
							commentManager.create(issue, appuser, j.get(commName), false)
						}
						} catch (all) {
							logger.error "Failed add comment - Issue: " + issue.getKey() + " TicketNumber: " + j.get("TicketNumber") + " Comment #: " + i.toString()
							logger.error all
						}
					}
				}
			}
			log.warn ("Processed issues $a")
			a++
		}
	}
}
