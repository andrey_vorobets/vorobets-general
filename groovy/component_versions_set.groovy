package com.meredith.jira.groovy
 
import org.apache.log4j.Category
 
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.bc.JiraServiceContext
import com.atlassian.jira.bc.JiraServiceContextImpl
import com.atlassian.jira.bc.filter.SearchRequestService
import com.atlassian.jira.event.issue.AbstractIssueEventListener
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.search.SearchRequest
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.web.bean.PagerFilter
 
class ComponentVersionsFromBundle extends AbstractIssueEventListener {
  Category log = Category.getInstance(ComponentVersionsFromBundle.class)
 
  private static final Long FILTER_ID = 10200
  ComponentManager cm = ComponentManager.getInstance()
  def cfm = cm.getCustomFieldManager()
  //CustomField workPriority = cfm.getCustomFieldObjectByName("Work Priority")
  def authenticationContext = cm.getJiraAuthenticationContext()
  JiraServiceContext ctx = new JiraServiceContextImpl(authenticationContext.getUser())
  def searchProvider = cm.getSearchProvider()
  SearchRequestService searchRequestService = cm.getSearchRequestService()
  //SearchRequest request = searchRequestService.getFilter(ctx, FILTER_ID)
  IssueManager issueManager = cm.getIssueManager()
 
  public ComponentVersionsFromBundle()
  {
    log.setLevel(org.apache.log4j.Level.ERROR)
    log.debug "debug statements"
    log.info "in constructor"
 
  }
  @Override
  void workflowEvent(IssueEvent event) {
    MutableIssue issue = event.issue as MutableIssue
    def currentIssuePriority = issue.getCustomFieldValue(workPriority)
    log.info "Original Issue: ${issue.id} - ${currentIssuePriority}"
// Get out quick if no work priority is assigned
/*
    if (currentIssuePriority == null) {
      return
    }
*/
/*  
  def results = getSearchResults(request)
    results.getIssues().eachWithIndex { iss , i ->
      issue = issueManager.getIssueObject(iss.id)
      i += 1
      log.info "Processing ${issue.id}"
      if (i > currentIssuePriority) {
        log.info "  Changing from ${issue.getCustomFieldValue(cf)} to ${i - 1}"
        //issue.setCustomFieldValue(cf, new Integer(i - 1))
      } else {
        println("  No change")
      }
 
    }
 
*/ 
  }

/* 
  SearchResults getSearchResults(SearchRequest sr) {
    return searchProvider.search(sr.getQuery(), authenticationContext.getUser(), PagerFilter.getUnlimitedFilter())
  }
*/ 
 
 
}



/* ############################################################################################################ */

explain analyze select p1."BUNDLE_ID", p1."COMPONENT_ID", p2."VERSION_ID", p3.vname, p4.cname, p3.description from 
"AO_102D86_BUNDLE_CONTENT" p1, "AO_102D86_CMP_VERSION_MAPPING" p2, projectversion p3, 
component p4, "AO_102D86_BUNDLE" p5, project p6
 where p5."ID"=p1."BUNDLE_ID" and p6.pkey=p5."PROJECT_KEY" and
p2."COMPONENT_ID"=p1."COMPONENT_ID" and p2."VERSION_ID"=p1."VERSION_ID" and p3.id=p2."VERSION_ID" and p4.id=p1."COMPONENT_ID"
 and p1."BUNDLE_ID" in ('2', '4', '6')

select distinct p3.vname as vname, p4.cname as cname, p3.description as description from \"AO_102D86_BUNDLE_CONTENT\" p1, \"AO_102D86_CMP_VERSION_MAPPING\" p2, projectversion p3, component p4, \"AO_102D86_BUNDLE\" p5, project p6 where p5.\"ID\"=p1.\"BUNDLE_ID\" and p6.pkey=p5.\"PROJECT_KEY\" and p2.\"COMPONENT_ID\"=p1.\"COMPONENT_ID\" and p2.\"VERSION_ID\"=p1.\"VERSION_ID\" and p3.id=p2.\"VERSION_ID\" and p4.id=p1.\"COMPONENT_ID\" and p1.\"BUNDLE_ID\" in 

/* ############################################################################################################ */

ToDo list
[{"id":"11","type":"todo"},{"id":"12","type":"todo"},{"id":"dxCore - 1.0.3-bin","type":"todo"}]
[{"id":"iOS-TO - 64.3 :iOS","type":"todo"},{"id":"Mobile Gateway - 2.2.3 :DxMobile regulation version 2.2.3","type":"todo"},{"id":"Mobile Gateway - 2.2.2 :DxMobile version 2.2.2","type":"todo"},{"id":"dxCore - 1.0.3-bin","type":"todo"},{"id":"dxCore - 1.0.3-bin","type":"todo"},{"id":"binClient - 1.1","type":"todo"}]

/* ############################################################################################################# */
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.link.IssueLink

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

String BundlesInRelease = "customfield_11202"
String ReleaseComponent = "customfield_11400"

import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject(BundlesInRelease) 
CustomField cfrc= customFieldManager.getCustomFieldObject(ReleaseComponent)

delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");

log.debug "Issue updated " + event.issue

def value = event.issue.getCustomFieldValue(cf)
if (value.toString()!="null") {
  log.debug("Bundles in Release: " + value)
  String jql_query="("
for (String dd in value.tokenize("###")) {
    log.debug("Bundle link value of budle id: " + dd.tokenize(". ")[0])
    jql_query=jql_query+"'"+dd.tokenize(". ")[0]+"',"
}
jql_query=jql_query[0..-2]+")"
log.debug("Bundle link JQL query: " + jql_query)
    
def sqlStmt = "select distinct p3.vname as vname, p4.cname as cname, p3.description as description from \"AO_102D86_BUNDLE_CONTENT\" p1, \"AO_102D86_CMP_VERSION_MAPPING\" p2, projectversion p3, component p4, \"AO_102D86_BUNDLE\" p5, project p6 where p5.\"ID\"=p1.\"BUNDLE_ID\" and p6.pkey=p5.\"PROJECT_KEY\" and p2.\"COMPONENT_ID\"=p1.\"COMPONENT_ID\" and p2.\"VERSION_ID\"=p1.\"VERSION_ID\" and p3.id=p2.\"VERSION_ID\" and p4.id=p1.\"COMPONENT_ID\" and p1.\"BUNDLE_ID\" in " + jql_query
log.debug("SQL query for component versions: " + sqlStmt)
    
Connection conn = ConnectionFactory.getConnection(helperName);
Sql sql = new Sql(conn)

String ReleaseComponentVersion = ""
String ReleaseComponentVersionJson = "["
sql.eachRow(sqlStmt) {row ->
    log.debug("$row.vname ($row.cname) ($row.description)")    
    ReleaseComponentVersion = row.cname.toString() + " - " + row.vname.toString()
    if (row.description.toString()!="null"){
        ReleaseComponentVersion = ReleaseComponentVersion + " :" + row.description.toString()
        }
    log.debug("ReleaseComponentVersion :" + ReleaseComponentVersion)
    ReleaseComponentVersionJson=ReleaseComponentVersionJson+"{\"id\":\""+ReleaseComponentVersion+"\",\"type\":\"todo\"},"

}
ReleaseComponentVersionJson=ReleaseComponentVersionJson[0..-2]+"]"
log.debug("JSON: " + ReleaseComponentVersionJson)

log.debug("Release actual value :" + event.issue.getCustomFieldValue(cfrc))

ModifiedValue mVal = new ModifiedValue(event.issue.getCustomFieldValue(cfrc), ReleaseComponentVersionJson)
log.debug("Update ReleaseComponentVersion - Modified value " + mVal)
cfrc.updateValue(null, event.issue, mVal, changeHolder)
log.debug("Release updated value :" + event.issue.getCustomFieldValue(cfrc))

} else {
    log.debug "BundleInRelease is null"
    }