//@Grab('org.apache.commons:commons-csv:1.2')
// CSV as TAB delimiter
// UTF-8 as encodong
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVFormat
import static org.apache.commons.csv.CSVFormat.*

import java.nio.file.Paths
import groovy.json.JsonOutput
import java.text.*
import java.sql.Timestamp
import java.util.Collection
import java.util.List

// https://commons.apache.org/proper/commons-csv/apidocs/org/apache/commons/csv/CSVRecord.html
// https://docs.atlassian.com/jira/6.4.1/com/atlassian/jira/user/util/UserUtil.html
// https://docs.atlassian.com/jira/latest/com/atlassian/jira/user/UserUtils.html
// https://developer.atlassian.com/static/javadoc/embedded-crowd-api/latest/reference/com/atlassian/crowd/embedded/api/User.html
import com.atlassian.jira.user.UserUtils
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.bc.user.search.UserPickerSearchService
import com.atlassian.jira.bc.user.search.UserSearchParams
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.project.Project
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.issue.IssueFactory
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
//import com.atlassian.jira.issue.priority.Priority
import com.atlassian.jira.issue.fields.config.FieldConfig
import com.atlassian.jira.issue.customfields.manager.OptionsManager

import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")

IssueManager issueMgr = ComponentAccessor.getIssueManager()
ProjectManager projectMgr = ComponentAccessor.getProjectManager()
UserUtil userUtil = ComponentAccessor.getUserUtil()
IssueFactory issueFactory = ComponentAccessor.getIssueFactory()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
OptionsManager optionsManager = ComponentAccessor.getOptionsManager()

String asUserName = "smrclient"
User user = userUtil.getUserObject(asUserName)
Project curPrj = projectMgr.getProjectObjByKey("SMRRO")

// Custom Fields
String emailAddress = "customfield_10100" // Email Address
String originalID = "customfield_14779" // Original ID
String inquirersName = "customfield_10904" // Inquirer's Name
String participantType = "customfield_15161" // Participant Type
String methodofInquiry = "customfield_10898" // Method of Inquiry
String payeeNumber = "customfield_15162" // Payee #
String accountNumber = "customfield_10916" // Account Number
String recordLabel = "customfield_15170" // Record Label(s)
String phoneNumber = "customfield_10906" // Phone
String resolvedDate = "customfield_10950" // Resolved Date
String artist = "customfield_10312" // Artist
String statementAdmin = "customfield_15171" // Statement Admin
String typeofRequest = "customfield_14781" // Type of Request

// Custom Field Objects
CustomField emailAddressObj= customFieldManager.getCustomFieldObject(emailAddress)
CustomField originalIDObj= customFieldManager.getCustomFieldObject(originalID)
CustomField inquirersNameObj= customFieldManager.getCustomFieldObject(inquirersName)
CustomField participantTypeObj= customFieldManager.getCustomFieldObject(participantType)
CustomField methodofInquiryObj = customFieldManager.getCustomFieldObject(methodofInquiry)
CustomField payeeNumberObj = customFieldManager.getCustomFieldObject(payeeNumber)
CustomField accountNumberObj = customFieldManager.getCustomFieldObject(accountNumber)
CustomField recordLabelObj = customFieldManager.getCustomFieldObject(recordLabel)
CustomField phoneNumberObj = customFieldManager.getCustomFieldObject(phoneNumber)
CustomField resolvedDateObj = customFieldManager.getCustomFieldObject(resolvedDate)
CustomField artistObj = customFieldManager.getCustomFieldObject(artist)
CustomField statementAdminObj = customFieldManager.getCustomFieldObject(statementAdmin)
CustomField typeofRequestObj = customFieldManager.getCustomFieldObject(typeofRequest)

def listing = []
Integer a = 0
def statusMapping = ['Open Unassigned':'Open', 'Open Assigned':'Open', 'In Progress Assigned':'Open', 'Closed/Resolved':'Closed', 'Reopened Assigned':'Open', 'Waiting on Royalty Participant':'Client Info Requested', 'Waiting on Sony Music':'Label Info Requested', 'With ES':'Open', 'Pending Further Review':'Client Info Received']
def priorityMapping = ['Normal':'6', 'High':'3', 'Urgent':'7']
def participantTypeMapping = ['Artist':'Artist', 'Publisher':'Publisher', 'Songwriter':'Union', 'Producer':'3rd Party', 'Licensee':'3rd Party', 'Multiple':'Artist/Publisher']
def methodofInquiryMapping = ['Web Form':'Email', 'Email':'Email', 'Phone':'Phone', 'Paper':'Paper']
//def recordLabelMapping = ['Catalog':'3rd Party']
def typeofRequestMapping = ['1099 Inquiries':['Tax','1099'],'Daily Foreign Vendor Wire Payment Report':['Non-Royalty Related',null], 'I am not a Sony Music Royaltor but I have a question for Sony Music':['Non-Royalty Related',null], 'Non RTA Daily Statement Notifications':['Non-Royalty Related',null], 'Other – Non-Royalty Related Requests':['Non-Royalty Related',null], 'Other-General Feedback for Sony Music':['General',null], 'Other-I am not a Sony Music royalty participant but I have a question for Sony Music':['General','Non-royaltor SME concerns'],'Returned/Undeliverable Statement':['Non-Royalty Related',null],'Royalty Accounting Inquiry':['Royalty Inquiry','Accounting Analysis'], 'Royalty Administration Inquiry':['Royalty Inquiry',null], 'Royalty Contract/Account Setup Request':['Set up','Account'], 'Royalty Helpdesk Inquiry':['Royalty Inquiry', null], 'Royalty Metadata Inquiry':['Royalty Inquiry', null], 'Royalty Participant Inquiry':['Royalty Inquiry', 'Vendor Inquiry'], 'Royalty Payee Inquiry-Change of Address':['COA Artist', null], 'Royalty Payee Inquiry-Change of Payee':['COP', null], 'Royalty Payee Inquiry-Change of Payment Method':['COPM', null], 'Royalty Payee Inquiry-Tax Related Question':['Tax', 'Tax Questions'], 'Royalty Payment Inquiry':['Royalty Inquiry', null], 'Royalty Payment Inquiry-Check Reissue Request':['Royalty Inquiry', null], 'Royalty Payment Inquiry-Missing Royalty Payment':['Royalty Inquiry', 'Missing Accountings'], 'Royalty Payment Inquiry-Royalty Payment Question':['Royalty Inquiry', null], 'Royalty Payment Inquiry-Unclaimed Property Request':['Royalty Inquiry', null], 'Royalty Statement Inquiry':['Royalty Inquiry', null], 'Royalty Statement Inquiry-Missing Royalty Statement':['Royalty Inquiry', 'Missing Accountings'], 'Royalty Statement Inquiry-Royalty Statement Question':['Royalty Inquiry', null], 'Royalty Vendor Change Inquiry':['Royalty Inquiry', 'Vendor Inquiry'], 'Royalty Vendor Change Request':['Set up', 'Vendor'], 'Royalty Vendor Setup Request':['Set up', 'Vendor'], 'RTA Notifications':['Non-Royalty Related', null], 'Sony Music eLink Consolidated Statement Report':['eLink', null], 'Sony Music eLink Inquiry-Access Requested':['eLink', 'Access'], 'Sony Music eLink Inquiry-Missing Royalty Payment Online':['eLink', 'Missing Statement'], 'Sony Music eLink Inquiry-Missing Royalty Statement Online':['eLink', 'Missing Statement'], 'Sony Music eLink Inquiry-Registered User Assistance Needed':['eLink', 'User Assistance'], 'Sony Music eLink Inquiry-Royalty Payment Question':['Royalty Inquiry', null], 'Sony Music eLink Inquiry-Royalty Statement Question':['Royalty Inquiry', null], 'Sony Music eLink Open Vendor eLink Review Changes':['Set up', 'Vendor'], 'Sony Music Left Over Statement Report':['Non-Royalty Related', null], 'Tax Lien/Release':['Tax', 'Lien/Release'] ]

class Logger {
    private File logFile = new File("/jira/tmp/csv_data.log")
    private String dateFormat = "dd.MM.yyyy;HH:mm:ss.SSS"
    private boolean printToConsole = false
 
    /**
     * Catch all defined logging levels, throw  MissingMethodException otherwise
     * @param name
     * @param args
     * @return
     */
    def methodMissing(String name, args) {
        def messsage = args[0]
        if (printToConsole) {
            println messsage
        }
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat)
        String date = formatter.format(new Date())
        switch (name.toLowerCase()) {
            case "trace":
                logFile << "${date} TRACE ${messsage}\n"
                break
            case "debug":
                logFile << "${date} DEBUG ${messsage}\n"
                break
            case "info":
                logFile << "${date} INFO  ${messsage}\n"
                break
            case "warn":
                logFile << "${date} WARN  ${messsage}\n"
                break
            case "error":
                logFile << "${date} ERROR ${messsage}\n"
                break
            default:
                throw new MissingMethodException(name, delegate, args)
        }
    }
}
def logger = new Logger()

Timestamp getTimestamp(String inputdate) {
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy kk:mm aa")
    Date parsedDate = df.parse(inputdate)
    Timestamp ts = new Timestamp(parsedDate.getTime())
    return ts
}

User trytoFindUser(String searchstring, User user) {
    def searchService = ComponentAccessor.getComponent(UserPickerSearchService)
    UserSearchParams userSearchParams = new UserSearchParams(true, true, true)
    String parsedsearch
    String secondPart
    try {
        if (searchstring.tokenize(" ").size()>0) {
            parsedsearch = searchstring.tokenize(".")[1]
            secondPart = searchstring.tokenize(".")[0]
            //println parsedsearch + " Second part: " + secondPart
        } else {
            parsedsearch = searchstring
        }
    } catch(all) {
        log.error ("Error in searchstring")
        return user
    }
    List<User> listofUsers = searchService.findUsers(parsedsearch, userSearchParams)
    //println listofUsers
    if (listofUsers.size()>=1) {
        listofUsers.each {
            //println it.getDisplayName()
            if (it.getDisplayName().tr('A-Z', 'a-z').contains(secondPart.tr('A-Z', 'a-z'))) {
                user=it
            }
        }
        //User foundUser = listofUsers[0]
        //foundUser.getDisplayName()
    }
    return user
}

//ArrayList participantOption = []
//Paths.get('d:/MIGRATION_DATA2.csv').withReader('windows-1250') { reader ->
//Paths.get('/jira/tmp/MIGRATION_DATA4.csv').withReader('ISO-8859-1') { reader ->
Paths.get('/jira/tmp/MigrationData3.csv').withReader('UTF-8') { reader ->
    //CSVParser csv = new CSVParser(reader, DEFAULT.withHeader())
    // Use TAB delimeter
    CSVParser csv = new CSVParser(reader, CSVFormat.TDF.withHeader().withQuote(null))

    for (j in csv.getRecords()) {
        //println (j.toMap() as Expando)
        j.toMap() as Expando
        // println j.get(2)
        logger.trace "Migration Data - ClientEmail: " + j.get("ClientEmail")
        logger.trace "Migration Data - Summary: " + j.get("Summary")
        logger.trace "Migration Data - DueDate: " + getTimestamp(j.get("DueDate"))
        // Original Number
        logger.trace "Original Number: " + j.get("TicketNumber").tokenize("-")[1].toLong()
        log.warn ("Processed issues $a")
        // /*
        Issue newissue = issueFactory.getIssue()
        newissue.setSummary (j.get("Summary").take(254))
        newissue.setProjectObject (curPrj)
        newissue.setIssueTypeId("9")
        newissue.description = " "
        // Assignee & Reporter
        try {
            if (j.get("CreatedBy").size()>1) {
                newissue.reporter = trytoFindUser(j.get("CreatedBy"),user)
            } else {
                 newissue.reporter = user
            }
        } catch (all) {
            logger.error "Error in set reporter"
            logger.error all
            newissue.reporter = user
        }
        try {
            if (j.get("Assignee").size()>1) {
                newissue.assignee = trytoFindUser(j.get("Assignee"),user)
            } else {
                newissue.assignee = user
            }
        } catch (all) {
            logger.error "Error in set assignee"
            logger.error all
            newissue.assignee = user
        }        
        
        try {
            newissue.setDueDate(getTimestamp(j.get("DueDate")))
        } catch (all) {
            log.error ("Can't parse DueDate in Migration Data")
        }
        try {
            newissue.setCreated(getTimestamp(j.get("CreatedOn")))
        } catch (all) {
            log.error ("Can't parse CreatedOn in Migration Data")
        }
        try {
            if (j.get("ClosedDate").size()>1) {
                newissue.setResolutionDate(getTimestamp(j.get("ClosedDate")))
            }
        } catch (all) {
            logger.error "Cat't set Resolution Date or Empty"
            logger.error all
        }
        // Last updated
        try {
            if (j.get("LastUpdated").size()>1) {
                newissue.setUpdated(getTimestamp(j.get("LastUpdated")))
            }
        } catch (all) {
            logger.error "Cat't set Last Updated Date or Empty"
            logger.error all
        }
        try {
            logger.trace "New number: " + j.get("TicketNumber").tokenize("-")[1].toLong()
            newissue.setNumber(j.get("TicketNumber").tokenize("-")[1].toLong())
        } catch (all) {
            logger.error "Can't set Issue ID"
            logger.error all
        }
        try {
            logger.trace "New Key: SMRRO-" + j.get("TicketNumber").tokenize("-")[1].toLong().toString()
            newissue.setKey("SMRRO-"+j.get("TicketNumber").tokenize("-")[1].toLong().toString())
        } catch (all) {
            logger.error "Can't set Issue KEY"
            logger.error all
        }
        try {
            logger.trace "Priority: " + priorityMapping[j.get("Priority")]
            newissue.setPriorityId(priorityMapping[j.get("Priority")])
        } catch (all) {
            logger.error "Can't set Priority"
            logger.error all
        }

        newissue.setCustomFieldValue(emailAddressObj, j.get("ClientEmail"))
        logger.trace "Custom field Email Address: " + j.get("ClientEmail")
        newissue.setCustomFieldValue(originalIDObj, j.get("TicketNumber"))
        logger.trace "Custom field Original ID: " + j.get("TicketNumber")
        newissue.setCustomFieldValue(inquirersNameObj, j.get("ClientName"))
        logger.trace "Custom field Inquirers Name: " + j.get("ClientName")
        newissue.setCustomFieldValue(payeeNumberObj, j.get("PayeeNumber"))
        logger.trace "Custom Field PayeeNumber: " + j.get("PayeeNumber")
        newissue.setCustomFieldValue(accountNumberObj, j.get("AccountNumber"))
        logger.trace "Custom Field AccountNumber: " + j.get("AccountNumber")
        newissue.setCustomFieldValue(phoneNumberObj, j.get("PhoneNumber"))
        logger.trace "Custom Field Phone: " + j.get("PhoneNumber")
        newissue.setCustomFieldValue(artistObj, j.get("Artist Name"))
        logger.trace "Custom Field Artist: " + j.get("Artist Name")

        //  Resolved Date
        try {
            if (j.get("ResolvedDate").size()>1) {
                newissue.setCustomFieldValue(resolvedDateObj, getTimestamp(j.get("ResolvedDate")))
            }
        } catch (all) {
            //logger.error "Can't set Resolved Date"
            //logger.error all
        }
        // Participant Type
        FieldConfig participantTypefieldConfig = participantTypeObj.getRelevantConfig(newissue)
        //OptionsManager optionsManager = ComponentManager.getComponentInstanceOfType(OptionsManager.class)
        //OptionsManager optionsManager = ComponentAccessor.getOptionsManager()
        try {
            if (j.get("ParticipantType").size()>1) {
                Object participantOption = optionsManager.getOptions(participantTypefieldConfig).getOptionForValue(participantTypeMapping[j.get("ParticipantType")],null)
                newissue.setCustomFieldValue(participantTypeObj, participantOption)
            }
        } catch (all) {
            logger.error "Can't set ParticipantType"
            logger.error all
        }
        // Method of Inquiry
        try {
            FieldConfig methodofInquiryfieldConfig = methodofInquiryObj.getRelevantConfig(newissue)
            if (j.get("MethodOfInquiry").size()>1) {
                Object methodofInquiryOption = optionsManager.getOptions(methodofInquiryfieldConfig).getOptionForValue(methodofInquiryMapping[j.get("MethodOfInquiry")],null)
                newissue.setCustomFieldValue(methodofInquiryObj, methodofInquiryOption)
            }
        } catch (all) {
            logger.error "Can't set Method of Inquiry"
            logger.error all
        }
        // Statement Admin
        try {
            FieldConfig statementAdminfieldConfig = statementAdminObj.getRelevantConfig(newissue)
            if (j.get("Statement Administrator").size()>1) {
                Collection statementAdminOption = [optionsManager.getOptions(statementAdminfieldConfig).getOptionForValue(j.get("Statement Administrator"),null)]
                newissue.setCustomFieldValue(statementAdminObj, statementAdminOption)
            }
        } catch (all) {
            logger.error "Can't set Statement Admin"
            logger.error all
        }
        // Record Label(s)
        Collection recordLabelOption = []
        try {
            FieldConfig recordLabelfieldConfig = recordLabelObj.getRelevantConfig(newissue)
            if (j.get("Copyright").tr('A-Z', 'a-z').contains("yes") || j.get("Copyright").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("Copyright",null))
                //Collection recordLabelOption = [optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("Copyright",null)]
            }
            if (j.get("Catalog").tr('A-Z', 'a-z').contains("yes") || j.get("Catalog").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("CMG",null))
            }
            if (j.get("Columbia").tr('A-Z', 'a-z').contains("yes") || j.get("Columbia").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("Columbia",null))
            }
            if (j.get("Copyright Label").tr('A-Z', 'a-z').contains("yes") || j.get("Copyright Label").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("Copyright",null))
            }
            if (j.get("Epic").tr('A-Z', 'a-z').contains("yes") || j.get("Epic").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("Epic",null))
            }
            if (j.get("Masterworks/SCI").tr('A-Z', 'a-z').contains("yes") || j.get("Masterworks/SCI").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("Masterworks",null))
            }
            if (j.get("Nashville").tr('A-Z', 'a-z').contains("yes") || j.get("Nashville").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("Nashville",null))
            }
            if (j.get("RCA Label").tr('A-Z', 'a-z').contains("yes") || j.get("RCA Label").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("RCA",null))
            }
            if (j.get("SMG").tr('A-Z', 'a-z').contains("yes") || j.get("SMG").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("SMG",null))
            }
            if (j.get("Third Party").tr('A-Z', 'a-z').contains("yes") || j.get("Third Party").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("3rd Party",null))
            }
            if (j.get("US Latin").tr('A-Z', 'a-z').contains("yes") || j.get("US Latin").tr('A-Z', 'a-z').contains("no")) {
                recordLabelOption.add(optionsManager.getOptions(recordLabelfieldConfig).getOptionForValue("Latin",null))
            }
            newissue.setCustomFieldValue(recordLabelObj, recordLabelOption)
        } catch (all) {
            logger.error "Can't set Record Label(s)"
            logger.error all
        }
        // Type of Request
        try {
            FieldConfig typeofRequestfieldConfig = typeofRequestObj.getRelevantConfig(newissue)
            if (j.get("IssueType").size()>1) {
                //Collection typeofRequestOption = []
                def typeofRequestOption = new HashMap()
                if (typeofRequestMapping[j.get("IssueType")][1]==null) {
                    //typeofRequestOption.add(optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[j.get("IssueType")][0].toString(),null))
                    typeofRequestOption.put(null, optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[j.get("IssueType")][0].toString(),null))
                } else {
                    //typeofRequestOption.add(optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[j.get("IssueType")][1].toString(),optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[j.get("IssueType")][0].toString(),null).getOptionId()))
                    typeofRequestOption.put(null, optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[j.get("IssueType")][0].toString(),null))
                    typeofRequestOption.put("1", optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[j.get("IssueType")][1].toString(),optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[j.get("IssueType")][0].toString(),null).getOptionId()))
                }
                newissue.setCustomFieldValue(typeofRequestObj, typeofRequestOption)
            }
        } catch (all) {
            logger.error "Can't set Type of Request"
            logger.error all
        }
        Issue created = issueMgr.createIssueObject(user, newissue)
        // Created issue log
        logger.debug "Issue Created: " + created.getKey()
        logger.debug "Created Issue - Email Address: " + created.getCustomFieldValue(emailAddressObj)
        logger.debug "Created Issue - Original ID: " + created.getCustomFieldValue(originalIDObj)
        logger.debug "Created Issue - Inquirers Name: " + created.getCustomFieldValue(inquirersNameObj)
        logger.debug "Created Issue - Participant Type: " + created.getCustomFieldValue(participantTypeObj)
        logger.debug "Created Issue - Method of Inquiry: " + created.getCustomFieldValue(methodofInquiryObj)
        logger.debug "Created Issue - Record Label: " + created.getCustomFieldValue(recordLabelObj)
        logger.debug "Created Issue - Statement Admin: " + created.getCustomFieldValue(statementAdminObj)
        logger.debug "Created Issue - Type of Request: " + created.getCustomFieldValue(typeofRequestObj)
        // */
        // Set ID
        /*
        MutableIssue issue = issueMgr.getIssueObject(created.getKey())
        issue.setNumber(j.get("TicketNumber").tokenize("-")[1].toLong())
        println "MutableIssue: " + issue.getKey()
        */
        log.warn ("Processed issues $a")
        a++
    }
}
//listing[1].RequestReceived
listing[]