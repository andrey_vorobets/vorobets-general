import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
log.debug("S2 start datetime issue : " + issue.key)
passesCondition = true

import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.ComponentManager

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()

CustomField cf= customFieldManager.getCustomFieldObject("customfield_11300")

String start_date = issue.getCustomFieldValue(cf).toString()

log.debug("S2 Start datetime: " + start_date)
if (start_date!="null") {
    passesCondition = false
    }
log.debug("Returning Condition: " + passesCondition)