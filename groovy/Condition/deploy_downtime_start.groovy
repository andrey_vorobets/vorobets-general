// Show Start downtime button only if issue doesn't have started downtime session
// "select count(id) from deploy_downtime where issueid='ISSUEID' and endtime is null" should be null
import org.apache.log4j.Category

import com.atlassian.jira.ComponentManager

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

ComponentManager componentManager = ComponentManager.getInstance()
delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");
Connection conn = ConnectionFactory.getConnection(helperName);

Sql sql = new Sql(conn)
def sqlStmt = "select count(id) as count from deploy_downtime where issueid='"+issue.id+"' and endtime is null"
log.debug ("Downtime start button condition: SQL query: " + sqlStmt)

passesCondition = true
String opened_session = ""
sql.eachRow(sqlStmt) {row ->
	opened_session = row.count.toString()
}
sql.close()
log.debug ("Downtime start button condition: Opened session: " + opened_session)

if (opened_session!="0") {
	passesCondition = false
}
log.debug("Downtime start button condition: Returning Condition: " + passesCondition)