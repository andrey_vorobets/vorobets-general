// Show Create User profile button if Component = 'Hiring' and issueType = "Access"
import org.apache.log4j.Category
passesCondition = true
boolean hiringPresent = false

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.issue.IssueManager

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.web.bean.PagerFilter

def Category log = Category.getInstance("com.onresolve.jira.groovy.PostFunction")
log.debug("ITENT Create Profile for : " + issue.key)
String userIdent = "customfield_11104"
def String adminUserName = "avorobets"

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField uifield= customFieldManager.getCustomFieldObject(userIdent)
SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
UserUtil userUtil = ComponentAccessor.getUserUtil()
IssueManager issueManager = componentManager.getIssueManager()

User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
if (!user) {
user = userUtil.getUserObject(adminUserName)
}

String issueType = issue.getIssueTypeObject().getName()
if (issueType=="Access") {
	issueComponents = issue.getComponents()
	//log.debug "ITENT Create Profile - Components: " + issueComponents
	// Components: [[name:Hiring, assigneetype:1, project:10700, description:null, id:10604, url:null, lead:ashvaiko]]

	issueComponents.each {
		if (it.name == "Hiring") {
			hiringPresent = true
		}
	}
	log.debug "ITENT Create Profile hiringPresent: " + hiringPresent

	if (hiringPresent==false) {
	    passesCondition = false
	} else {
		jqlSL = ["project=ITENT and \"User Identifier\" = "+issue.getCustomFieldValue(uifield).getName()+" and issuetype=\"User profile\""]
		jqlSL.each {
			log.debug "ITENT Create Profile JQL String: " + it
			SearchService.ParseResult parseResult = searchService.parseQuery(user, it)
			if (parseResult.isValid()) {
				def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
				issues = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
				log.debug "ITENT Create Profile JQL Result Map size: " + issues.size()
				if (issues.size()>0) {
					passesCondition = false
				}
			} else {
	   			log.debug("Invalid JQL: " + it)
	    	}
    	}
	}
	log.debug "ITENT Create Profile passesCondition: " + passesCondition
} else {
	passesCondition = false
}