import com.atlassian.jira.component.ComponentAccessor
 
def groupManager = ComponentAccessor.getGroupManager()
!(groupManager.isUserInGroup(issue.assignee?.name, 'groupname'))

/* the same for advanced script below */

import com.atlassian.jira.component.ComponentAccessor
passesCondition = true
def groupManager = ComponentAccessor.getGroupManager()
if (groupManager.isUserInGroup(issue.assignee?.name, 'jira-administrators')){
    passesCondition = false
}