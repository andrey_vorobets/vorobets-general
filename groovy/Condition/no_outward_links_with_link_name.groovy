import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.ComponentManager
import org.apache.log4j.Category

String linkname="Release"

issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

int link_counter = 0
passesCondition = true
for (IssueLink link in issueLinkManager.getOutwardLinks(issue.getId())){
    log.debug("Condition - check links by name debug : " + link.getIssueLinkType().getName())
    if (link.getIssueLinkType().getName()==linkname){
        link_counter=link_counter+1
        }
    }
if (link_counter>0){
    passesCondition = false
    }
log.debug("Returning Condition: " + passesCondition)