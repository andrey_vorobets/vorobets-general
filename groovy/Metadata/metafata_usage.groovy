import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.osoboo.jira.metadata.MetadataService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.customfields.manager.OptionsManager
import com.atlassian.jira.project.Project

@WithPlugin("com.osoboo.jira-metadata-plugin")

@PluginModule
MetadataService metadataService

Project project = ComponentAccessor.getProjectManager().getProjectObjByKey("GSR")
def alist = metadataService.getMetadata(project)

log.error alist.size()