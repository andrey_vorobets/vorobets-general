import com.atlassian.jira.component.ComponentAccessor
//import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
//import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.Issue
import org.apache.log4j.Category

def Category log = Category.getInstance("com.onresolve.jira.groovy.CF.get_epic")

//ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
Issue issue_linked_to_epic

CustomField epicName = customFieldManager.getCustomFieldObjectByName('Epic Link')
CustomField epicName2 = customFieldManager.getCustomFieldObjectByName('Epic Name')

Issue issueParent = issue.getParentObject()
if (issueParent == null){
	issue_linked_to_epic = issue
} else {
	issue_linked_to_epic = issueParent
}

//MutableIssue issue = issueManager.getIssueObject("SAND-42")
try {
	Issue epic = issueManager.getIssueObject(issue_linked_to_epic.getCustomFieldValue(epicName).toString())
	if (epic.getCustomFieldValue(epicName2)) {
		return epic.getCustomFieldValue(epicName2)
	} else {
		return null
	}
} catch (all) {
	log.warn all
}