// Script workflow function : Fires an event under specified user that can be picked up by a notification scheme, in order to send mail only under certain conditions, eg Priority is Blocker. User:esroyalties, event:Notify Support Group.
issue.getComponentObjects()*.getName().contains('E-Link Analytics')
// Script workflow function : Fires an event under specified user that can be picked up by a notification scheme, in order to send mail only under certain conditions, eg Priority is Blocker. User:esroyalties, event:New issue created.
!isUserMemberOfRole('Developers') && !issue.getComponentObjects()*.getName().contains('E-Link Analytics')
// Notify scheme
// Notify Support Group	
//Group Custom Field Value (Business Unit Group)
//Group (ESR-Users-Analytics)