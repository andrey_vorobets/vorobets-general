import groovy.json.JsonSlurper

def parseJSON(text)
{
    def slurper = new JsonSlurper()
    def result = slurper.parseText(text)
    
    return result
}

def authString = "user:password".getBytes().encodeBase64().toString()
def urlString = "http://8.8.8.8/testrail/index.php?/api/v2/update_case/96372"
println urlString

def conn = urlString.toURL().openConnection()
println conn
conn.setRequestProperty( "Authorization", "Basic ${authString}" )
conn.setRequestProperty( "Content-Type", "application/json" )
//conn.connect()
//println conn.responseCode
///*
conn.setRequestMethod("POST")
conn.doOutput = true

///*
def writer = new OutputStreamWriter(conn.outputStream)
//def queryString = "{\"refs\":SAND-6}"
//def queryString = "{\"priority_id\": 1,	\"estimate\": \"15m\", \"refs\": \"SAND-6, SAND-21, SAND-4\"}"
def queryString = "{\"refs\": \"SAND-4, SAND-6\"}"
writer.write(queryString)
writer.flush()
writer.close()
//*/
conn.connect()
println conn.responseCode

def response = conn.content.text
println response
//*/
/*
println conn.responseCode
result = parseJSON(conn.content.text)
println result
*/