import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.preferences.UserPreferencesManager
import com.atlassian.jira.user.preferences.ExtendedPreferences
import com.atlassian.jira.user.preferences.PreferenceKeys

UserPreferencesManager userPreferencesManager = ComponentAccessor.getUserPreferencesManager()
ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

ExtendedPreferences extPref = userPreferencesManager.getExtendedPreferences(currentUser)
log.error("timeZone: " + extPref.getString(PreferenceKeys.USER_TIMEZONE))