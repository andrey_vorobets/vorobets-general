import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter

import com.atlassian.crowd.embedded.api.User

import com.atlassian.jira.util.ImportUtils
import com.atlassian.jira.issue.index.IssueIndexManager
import com.atlassian.jira.util.BuildUtils
import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

// Where we need to initialize 	DoR Warning field
String jql_query = "project=SAND and \"DoR Warning\" is EMPTY and issuetype not in subTaskIssueTypes()"

def user = ComponentAccessor.getJiraAuthenticationContext().getUser()
authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
currentUser = authenticationContext.getLoggedInUser()

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = ComponentAccessor.getIssueManager()
// In dev 11700, prod 11006
String DoRWarning = "customfield_11700"
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject(DoRWarning) 

delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");
Connection conn = ConnectionFactory.getConnection(helperName);

def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
SearchProvider searchProvider = ComponentAccessor.getComponentOfType(SearchProvider.class)

IssueIndexManager indexManager = ComponentManager.getInstance().getIndexManager()
boolean wasIndexing = ImportUtils.isIndexIssues()
ImportUtils.setIndexIssues(true)
log.error ("Current Build No " + BuildUtils.getCurrentBuildNumber() )


def query = jqlQueryParser.parseQuery(jql_query)
SearchResults results = searchProvider.search(query, user, PagerFilter.getUnlimitedFilter())

Sql sql = new Sql(conn)
Long res_number = 0
String dor_res = ""
results.getIssues().each {documentIssue ->
	def fissue = issueManager.getIssueObject(documentIssue.id)
	res_number = res_number +1
	def sqlStmt = "select * from dor_status('"+fissue.id+"')"
	
	sql.eachRow(sqlStmt) {row ->
		//dor_res = dor_res+"\n\n"+row.dor.toString()
		dor_res = row.dor.toString()
		cf.updateValue(null, fissue, new ModifiedValue(fissue.getCustomFieldValue(cf), dor_res),changeHolder)
		indexManager.reIndex(issueManager.getIssue(fissue.id))
	}
}
sql.close()
ImportUtils.setIndexIssues(wasIndexing)

//dor_res
res_number