//import groovy.json.*
import groovy.json.JsonSlurper

def parseJSON(text)
{
    def slurper = new JsonSlurper()
    def result = slurper.parseText(text)
    
    return result
}

def getResult(addr, authString) {
    def conn = addr.toURL().openConnection()
    conn.setRequestProperty( "Authorization", "Basic ${authString}" )
    conn.setRequestProperty( "Content-Type", "application/json" )
    conn.connect()
    println conn.responseCode
    if( conn.responseCode == 200 ) {
        def result = parseJSON(conn.content.text)
        return result
    }
}

def authString = "username:password".getBytes().encodeBase64().toString()
def urlRoot = "http://192.168.2.159/testrail/index.php?"
// get All projects
def proj_addr       = "${urlRoot}/api/v2/get_projects"
println proj_addr
def result = getResult(proj_addr, authString)
//println "json :${result}"
// index i if you whant to count how many projects you have
result.eachWithIndex { it, i ->
    //println "$i: $it"
    print "Project ID: "
    println it.get("id")
    def proj_id=it.get("id")
    def suites = "${urlRoot}/api/v2/get_suites/${proj_id}"
    println suites
    def suit_result=getResult(suites, authString)
    //println "json :${suit_result}"
    suit_result.eachWithIndex { it2, i2 ->
        print "Suite ID: "
        println it2.get("id")
        def suite_id = it2.get("id")
        def cases = "${urlRoot}/api/v2/get_cases/${proj_id}&suite_id=${suite_id}"
        println cases
        def cases_result=getResult(cases, authString)
        //println "json :${cases_result}"
        cases_result.eachWithIndex { it3, i3 ->
            def refs = it3.get("refs")
            if (refs.toString()!="null") {
                //println "json :${cases_result}"
                print "Case ID: "
                println it3.get("id")
                println refs
            }
        }
    }
}