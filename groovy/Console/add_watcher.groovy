import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.watchers.WatcherManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.util.UserUtil

import com.atlassian.jira.util.ImportUtils
import com.atlassian.jira.issue.index.IssueIndexManager
import com.atlassian.jira.util.BuildUtils

import org.apache.log4j.Category
def Category log = Category.getInstance("com.onresolve.jira.groovy.AddWatcher")

String jql_query = "project=SAND and key='SAND-69'"
String asUserName = "avorobets"


def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
SearchProvider searchProvider = ComponentAccessor.getComponentOfType(SearchProvider.class)
IssueManager issueManager = ComponentAccessor.getIssueManager()

def user = ComponentAccessor.getJiraAuthenticationContext().getUser()
def authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
def currentUser = authenticationContext.getUser()
WatcherManager watcherManager = ComponentAccessor.getWatcherManager()

IssueIndexManager indexManager = ComponentAccessor.getIssueIndexManager()
boolean wasIndexing = ImportUtils.isIndexIssues()
ImportUtils.setIndexIssues(true)
UserUtil userUtil = ComponentAccessor.getUserUtil()

ApplicationUser appuser = userUtil.getUserByName(asUserName)

def query = jqlQueryParser.parseQuery(jql_query)
SearchResults results = searchProvider.search(query, user, PagerFilter.getUnlimitedFilter())
results.getIssues().each {documentIssue ->
	def fissue = issueManager.getIssueObject(documentIssue.id)
	log.error "Issue Key: " + fissue.getKey()
	watcherManager.startWatching(appuser, fissue)
	indexManager.reIndex(fissue)
}

ImportUtils.setIndexIssues(wasIndexing)