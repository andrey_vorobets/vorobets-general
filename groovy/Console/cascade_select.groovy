import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.config.FieldConfig
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.customfields.manager.OptionsManager

import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

//ComponentManager componentManager = ComponentManager.getInstance()
//IssueManager issueManager = ComponentAccessor.getIssueManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()

String DoRWarning = "customfield_14781"
String typeofRequest = "customfield_14781" // Type of Request
//CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
//CustomField cf= customFieldManager.getCustomFieldObject(DoRWarning) 
CustomField typeofRequestObj = customFieldManager.getCustomFieldObject(typeofRequest)
OptionsManager optionsManager = ComponentAccessor.getOptionsManager()
IssueChangeHolder changeHolder = new DefaultIssueChangeHolder()

MutableIssue issue = issueManager.getIssueObject("SMRRO-29")
issue.id

String zzz="Royalty Payee Inquiry-Tax Related Question"
def typeofRequestMapping = ['Royalty Payee Inquiry-Tax Related Question':['Tax', 'Tax Questions'] ]


def typeofRequestOption = new HashMap()
FieldConfig typeofRequestfieldConfig = typeofRequestObj.getRelevantConfig(issue)
//typeofRequestOption.add(optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[j.get("IssueType")][0].toString(),null))
def parentOptionObj = optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[zzz][0].toString(),null)
//def childOptionObj = optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[zzz][1].toString(),null)
def childOptionObj = optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[zzz][1].toString(),optionsManager.getOptions(typeofRequestfieldConfig).getOptionForValue(typeofRequestMapping[zzz][0].toString(),null).getOptionId())
println "parentOptionObj: " + parentOptionObj
println "childOptionObj: " + childOptionObj

//newValues.put(null, parentOptionObj)
//newValues.put("1", childOptionObj)
typeofRequestOption.put(null, parentOptionObj)
typeofRequestOption.put("1", childOptionObj)
typeofRequestObj.updateValue(null, issue, new ModifiedValue(null, typeofRequestOption), changeHolder)