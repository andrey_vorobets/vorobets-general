import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.link.IssueLink

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

// Bundles in Release - Database Information custom field
String BundlesInRelease = "customfield_10902"
// Release components versions - ToDo List custom field 
String ReleaseComponent = "customfield_10803"

import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject(BundlesInRelease) 
CustomField cfrc= customFieldManager.getCustomFieldObject(ReleaseComponent)

delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");

log.debug "Issue updated " + event.issue

def value = event.issue.getCustomFieldValue(cf)
if (value.toString()!="null") {
  log.debug("Bundles in Release: " + value)
  String jql_query="("
for (String dd in value.tokenize("###")) {
    log.debug("Bundle link value of budle id: " + dd.tokenize(". ")[0])
    jql_query=jql_query+"'"+dd.tokenize(". ")[0]+"',"
}
jql_query=jql_query[0..-2]+")"
log.debug("Bundle link JQL query: " + jql_query)
    
def sqlStmt = "select distinct p3.vname as vname, p4.cname as cname, p3.description as description from \"AO_102D86_BUNDLE_CONTENT\" p1, \"AO_102D86_CMP_VERSION_MAPPING\" p2, projectversion p3, component p4, \"AO_102D86_BUNDLE\" p5, project p6 where p5.\"ID\"=p1.\"BUNDLE_ID\" and p6.pkey=p5.\"PROJECT_KEY\" and p2.\"COMPONENT_ID\"=p1.\"COMPONENT_ID\" and p2.\"VERSION_ID\"=p1.\"VERSION_ID\" and p3.id=p2.\"VERSION_ID\" and p4.id=p1.\"COMPONENT_ID\" and p1.\"BUNDLE_ID\" in " + jql_query
log.debug("SQL query for component versions: " + sqlStmt)
    
Connection conn = ConnectionFactory.getConnection(helperName);
Sql sql = new Sql(conn)

String ReleaseComponentVersion = ""
String ReleaseComponentVersionJson = "["
sql.eachRow(sqlStmt) {row ->
    log.debug("$row.vname ($row.cname) ($row.description)")    
    ReleaseComponentVersion = row.cname.toString() + " - " + row.vname.toString()
    if (row.description.toString()!="null"){
        ReleaseComponentVersion = ReleaseComponentVersion + " :" + row.description.toString()
        }
    log.debug("ReleaseComponentVersion :" + ReleaseComponentVersion)
    ReleaseComponentVersionJson=ReleaseComponentVersionJson+"{\"id\":\""+ReleaseComponentVersion+"\",\"type\":\"todo\"},"

}
sql.close()
ReleaseComponentVersionJson=ReleaseComponentVersionJson[0..-2]+"]"
log.debug("JSON: " + ReleaseComponentVersionJson)

log.debug("Release actual value :" + event.issue.getCustomFieldValue(cfrc))

ModifiedValue mVal = new ModifiedValue(event.issue.getCustomFieldValue(cfrc), ReleaseComponentVersionJson)
log.debug("Update ReleaseComponentVersion - Modified value " + mVal)
cfrc.updateValue(null, event.issue, mVal, changeHolder)
log.debug("Release updated value :" + event.issue.getCustomFieldValue(cfrc))

} else {
    log.debug "BundleInRelease is null"
    }