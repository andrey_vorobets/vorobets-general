import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkManager

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter

import com.atlassian.crowd.embedded.api.User

import org.apache.log4j.Category

// Bundles in Release - Database Information custom field 
// In dev 11202, prod 10902
String BundlesInRelease = "customfield_10902"
// Release components versions - ToDo List custom field 
// In dev 11400, prod 10803
String ReleaseComponent = "customfield_10803"
// Relates link (the same in dev & prod: 10003)
Long link_id = 10003
// Dev Release Notes
// In dev & prod 10502
String DevReleaseNotes = "customfield_10502"
// RD: Configuration Notes
// In dev 11600 & prod 10903
String ConfugurationNotes = "customfield_10903"

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
// log.error "Debug message"
//log.setLevel(org.apache.log4j.Level.DEBUG)

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
ComponentManager componentManager = ComponentManager.getInstance()
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject(BundlesInRelease) 
CustomField cfrc= customFieldManager.getCustomFieldObject(ReleaseComponent)

delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");

log.debug "Issue updated " + event.issue

def value = event.issue.getCustomFieldValue(cf)
if (value.toString()!="null") {
    log.debug("Bundles in Release: " + value)
    String s_query="("
    for (String dd in value.tokenize("###")) {
        log.debug("Bundle link value of budle id: " + dd.tokenize(". ")[0])
        s_query=s_query+"'"+dd.tokenize(". ")[0]+"',"
    }
    s_query=s_query[0..-2]+")"
    //log.debug("Bundle link SQL query: " + s_query)
    
def sqlStmt = "select distinct p3.vname as vname, p4.cname as cname, p3.description as description from \"AO_102D86_BUNDLE_CONTENT\" p1, \"AO_102D86_CMP_VERSION_MAPPING\" p2, projectversion p3, component p4, \"AO_102D86_BUNDLE\" p5, project p6 where p5.\"ID\"=p1.\"BUNDLE_ID\" and p6.pkey=p5.\"PROJECT_KEY\" and p2.\"COMPONENT_ID\"=p1.\"COMPONENT_ID\" and p2.\"VERSION_ID\"=p1.\"VERSION_ID\" and p3.id=p2.\"VERSION_ID\" and p4.id=p1.\"COMPONENT_ID\" and p1.\"BUNDLE_ID\" in " + s_query
    log.debug("SQL query for component versions: " + sqlStmt)
    
Connection conn = ConnectionFactory.getConnection(helperName);
Sql sql = new Sql(conn)

String ReleaseComponentVersion = ""
String ReleaseComponentVersionJson = "["
sql.eachRow(sqlStmt) {row ->
    log.debug("$row.vname ($row.cname) ($row.description)")    
    ReleaseComponentVersion = row.cname.toString() + " - " + row.vname.toString()
    if (row.description.toString()!="null"){
        ReleaseComponentVersion = ReleaseComponentVersion + " :" + row.description.toString()
        }
    log.debug("ReleaseComponentVersion :" + ReleaseComponentVersion)
    ReleaseComponentVersionJson=ReleaseComponentVersionJson+"{\"id\":\""+ReleaseComponentVersion+"\",\"type\":\"todo\"},"

}
sql.close()
if (ReleaseComponentVersionJson.length()>3) {
    ReleaseComponentVersionJson=ReleaseComponentVersionJson[0..-2]+"]"
    log.debug("JSON: " + ReleaseComponentVersionJson)

    //log.debug("Release actual value :" + event.issue.getCustomFieldValue(cfrc))

    ModifiedValue mVal = new ModifiedValue(event.issue.getCustomFieldValue(cfrc), ReleaseComponentVersionJson)
    //log.debug("Update ReleaseComponentVersion - Modified value " + mVal)
    cfrc.updateValue(null, event.issue, mVal, changeHolder)
    log.debug("Release updated value :" + event.issue.getCustomFieldValue(cfrc))
}

def user = ComponentAccessor.getJiraAuthenticationContext().getUser()
authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
currentUser = authenticationContext.getLoggedInUser()
log.debug("currentUser :" + currentUser)

// Unlink Relates issues
linkMgr = ComponentManager.getInstance().getIssueLinkManager()
for (IssueLink link in linkMgr.getOutwardLinks(event.issue.id)) {
    //log.debug("Unlink - check links by name : " + link.getIssueLinkType().getName())
    if (link.getLinkTypeId()==link_id){
        //log.debug("Unlink - Link ID:" + link.getLinkTypeId().toString())
        linkMgr.removeIssueLink(link, currentUser)
    }
}

// Link issues

String jql_query="issue in fixedInBundle("
for (String dd in value.tokenize("###")) {
    log.debug("Bundle link value of budle id: " + dd.tokenize(". ")[0])
    jql_query=jql_query+dd.tokenize(". ")[0]+","
}
if (jql_query.length()>24) {
    jql_query=jql_query[0..-2]+")"
    log.debug("Bundle link JQL query: " + jql_query)

    def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
    SearchProvider searchProvider = ComponentAccessor.getComponentOfType(SearchProvider.class)
    def issueManager = ComponentAccessor.getIssueManager()

    def query = jqlQueryParser.parseQuery(jql_query)
    SearchResults results = searchProvider.search(query, user, PagerFilter.getUnlimitedFilter())
    IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()

    results.getIssues().each {documentIssue ->
        // if you need a mutable issue you can do:
        def fissue = issueManager.getIssueObject(documentIssue.id)
        // do something to the issue...
        //log.debug("Link Follow Issues:")
        //log.debug(fissue.summary)
        //log.debug("End of linking Issues")
        issueLinkManager.createIssueLink(event.issue.getId(), fissue.getId(), link_id, null, currentUser)
    }
}

// Grab Dev Release Notes

String confnotes = ""
CustomField cfdr= customFieldManager.getCustomFieldObject(DevReleaseNotes)
CustomField confnt= customFieldManager.getCustomFieldObject(ConfugurationNotes)

for (IssueLink link in linkMgr.getOutwardLinks(event.issue.id)) {
 
     def labEq = link.getDestinationObject().getCustomFieldValue(cfdr).toString()
     //log.debug("Dev Release note for issue: " + link.getDestinationObject().getKey().toString())
        if (labEq!="null") {
                confnotes = confnotes + "\\[" + link.getDestinationObject().getKey().toString() + "\\] " + link.getDestinationObject().getSummary()+ "\n\n" + labEq + "         \n\n"
            }
}
log.debug("Concatenated Notes:" + confnotes)
//event.issue.setCustomFieldValue(confnt, confnotes)

ModifiedValue BVal = new ModifiedValue(event.issue.getCustomFieldValue(confnt), confnotes)
//log.debug("Update ReleaseComponentVersion - Modified value " + BVal)
confnt.updateValue(null, event.issue, BVal, changeHolder)
//log.debug("Configuration Notes updated value :" + event.issue.getCustomFieldValue(confnt))

} else {
    // Feature or Bug - on Empty Bundle we do nothing with issue
    log.debug "BundleInRelease is null"
}