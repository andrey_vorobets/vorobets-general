import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
//import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField
//import com.atlassian.jira.issue.customfields.option.Options
//import com.atlassian.jira.issue.customfields.option.Option

import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

import groovy.sql.Sql
import java.sql.Connection
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = ComponentAccessor.getIssueManager()

// DoR Warning
// In dev 11700, prod 11006
String DoRWarning = "customfield_11006"
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()
CustomField cf= customFieldManager.getCustomFieldObject(DoRWarning) 

//MutableIssue issue = issueManager.getIssueObject("SAND-48")
//Options options = cf.getOptions(null, cf.getRelevantConfig(issue), null)

delegator = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
String helperName = delegator.getGroupHelperName("default");
Connection conn = ConnectionFactory.getConnection(helperName);

Sql sql = new Sql(conn)
def sqlStmt = "select * from dor_status('"+event.issue.id+"')"
String dor_res = ""
sql.eachRow(sqlStmt) {row ->
dor_res = row.dor.toString()
}
sql.close()
dor_res
cf.updateValue(null, event.issue, new ModifiedValue(event.issue.getCustomFieldValue(cf), dor_res),changeHolder)