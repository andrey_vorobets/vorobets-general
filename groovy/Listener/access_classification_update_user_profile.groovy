import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.crowd.embedded.api.User

import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.fields.CustomField

import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.web.bean.PagerFilter

import com.atlassian.jira.issue.link.IssueLink

import org.apache.log4j.Category
/* What we do:
1. Select users from the "Users Identifier"  field
2. For each user try to find issue in the ISSEC project with "User profile" issue type & "User Identifier" field must be equal to the founded user
3. Check if this founded issue have link with the initial issue "User dependencies"
4. IF link is not presenet - create new issue link from founded issue to the initial issue
*/

IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
ComponentManager componentManager = ComponentManager.getInstance()
IssueManager issueManager = ComponentAccessor.getIssueManager()
SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
CustomFieldManager customFieldManager = componentManager.getCustomFieldManager()

issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()

User cwdUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
// Access classification Kyiv
String AccClass = "customfield_11107"
// Users Identifier
String UsersIdent = "customfield_11200"
// USer dependencies link id
Long link_id = 10600

CustomField f_accclass= customFieldManager.getCustomFieldObject(AccClass)
CustomField f_usersident= customFieldManager.getCustomFieldObject(UsersIdent)

log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")
log.debug "Access classification - Issue updated: " + event.issue

log.debug "Access classification - Users identifier: " + event.issue.getCustomFieldValue(f_usersident)
// [avorobets(avorobets), ashvaiko(ashvaiko)]
log.debug "Access classification - Access Class field: " + event.issue.getCustomFieldValue(f_accclass)
// Access Class field[Laptop, Network]

// by users
Integer counter = 0
event.issue.getCustomFieldValue(f_usersident).each { user_ident_list ->
	// try to find user profile
	jqlSL = ["project=\"ISSEC\" and issuetype=\"User profile\" and \"User Identifier\"="+user_ident_list.getName()]
	log.debug "JQL String: " + jqlSL
	jqlSL.each {
		SearchService.ParseResult parseResult = searchService.parseQuery(cwdUser, it)
		if (parseResult.isValid()) {
			def searchResult = searchService.search(cwdUser, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
			issues = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
			//log.debug "Users identifier search JQL Result Map size: " + issues.size()
			if (issues.size()>0) {
				for ( fissue in issues ){
					counter = 0
					//log.debug "Founded User profile issue key: " + fissue.getKey()
					for (IssueLink link in issueLinkManager.getInwardLinks(fissue.getId())){
						//log.debug("Check links by name : " + link.getIssueLinkType().getName())
						//log.debug("Check links by target key : " + link.getSourceObject().getKey())
						if (link.getSourceObject().getKey()==event.issue.getKey()) {
							counter = counter + 1
						}
					}
					//log.debug "counter = " + counter
					if (counter == 0 ) {
						log.debug "Issue " + fissue.getKey() + " is Not linked to the initial issue " + event.issue.getKey()
						issueLinkManager.createIssueLink(event.issue.getId(), fissue.getId(), link_id, null, cwdUser)
					}
				}
			}
		} else {
			log.debug("Invalid JQL: " + it)
		}
	}
}