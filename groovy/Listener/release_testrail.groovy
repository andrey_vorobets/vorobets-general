import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkManager

//import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
//import com.atlassian.jira.issue.util.IssueChangeHolder

import groovy.json.JsonSlurper

import org.apache.log4j.Category
log = Category.getInstance("com.onresolve.jira.groovy.LinkedIssues")

log.debug "Test integration with Test Rail " + event.issue
String linkname="Relates"
def authString = "user:password".getBytes().encodeBase64().toString()
def urlRoot = "http://8.8.8.8/testrail/index.php?"
def slurper = new JsonSlurper()

def parseJSON(text, slurper)
{    
    def result = slurper.parseText(text)    
    return result
}

def getResult(addr, authString, slurper) {
    def conn = addr.toURL().openConnection()
    conn.setRequestProperty( "Authorization", "Basic ${authString}" )
    conn.setRequestProperty( "Content-Type", "application/json" )
    conn.connect()
    //println conn.responseCode
    if( conn.responseCode == 200 ) {
        def result = parseJSON(conn.content.text, slurper)
        return result
    }
}

def checkRefs(IssuesMap, RefMap) {
	def result = 0
	RefMap.each {
		if (IssuesMap.indexOf(it) >= 0){
			result = 1
		}		
	}
	return result
}

def updateCase(addr, refs, authString) {
	def conn = addr.toURL().openConnection()
	conn.setRequestProperty( "Authorization", "Basic ${authString}" )
	conn.setRequestProperty( "Content-Type", "application/json" )
	conn.setRequestMethod("POST")
	conn.doOutput = true
	def writer = new OutputStreamWriter(conn.outputStream)
	def queryString = "{\"refs\": \""+refs+"\"}"
	writer.write(queryString)
	writer.flush()
	writer.close()
	conn.connect()
	if( conn.responseCode == 200 ) {
		def response = conn.content.text
		log.debug("TestRail: Update Case response : " + response)
	}
}

ComponentManager componentManager = ComponentManager.getInstance()
issueLinkManager = ComponentManager.getInstance().getIssueLinkManager()
def IssuesMap = []

int link_counter = 0
for (IssueLink link in issueLinkManager.getOutwardLinks(event.issue.getId())){
	log.debug("TestRail: Link by name : " + link.getIssueLinkType().getName())
    if (link.getIssueLinkType().getName()==linkname){
    	log.debug("TestRail: Linked Issue " + link.getDestinationObject().getKey().toString())
    	IssuesMap.add(link.getDestinationObject().getKey().toString())
        link_counter=link_counter+1
        }
}
log.debug("TestRail: Found Relates links:" + link_counter)
log.debug("TestRail: Issues Map:" + IssuesMap)
// Issues Map:[CFD-1004, DEAL-1514]

// get All projects
def casesMap = []
def proj_addr       = "${urlRoot}/api/v2/get_projects"
log.debug("TestRail: Projects Rest URL:" + proj_addr)
def result = getResult(proj_addr, authString, slurper)
def checkRefsInIssues = 0
//println "json :${result}"
// index i if you whant to count how many projects you have
result.eachWithIndex { it, i ->
    //println "$i: $it"
    log.debug "TestRail: Project ID:" + it.get("id")
    def proj_id=it.get("id")

    // Suites
    ///*
    def suites = "${urlRoot}/api/v2/get_suites/${proj_id}"
    log.debug "TestRail: suites rest url: " + suites
    def suit_result=getResult(suites, authString, slurper)
    //println "json :${suit_result}"
    suit_result.eachWithIndex { it2, i2 ->
        //log.debug "TestRail: Suite ID: " + it2.get("id")
        def suite_id = it2.get("id")
        def cases = "${urlRoot}/api/v2/get_cases/${proj_id}&suite_id=${suite_id}"
        //log.debug "TestRail: Case ID: " + cases
        def cases_result=getResult(cases, authString, slurper)
        //println "json :${cases_result}"
        cases_result.eachWithIndex { it3, i3 ->
            def refs = it3.get("refs")
            if (refs.toString()!="null") {
                //println "json :${cases_result}"
                //log.debug "TestRail: Case ID: " + it3.get("id") + " Refs: " + refs
                //log.debug "TestRail: Case Refs tokenize: " + refs.tokenize(", ")
                checkRefsInIssues = checkRefs(IssuesMap, refs.tokenize(", "))
                //log.debug "TestRail: Case Ref presence in Issues Map: " + checkRefsInIssues
                if (checkRefsInIssues==1){
                	log.debug "TestRail: Case ID: " + it3.get("id") + " Refs: " + refs
                	//log.debug "TestRail: Case Refs tokenize: " + refs.tokenize(", ")
                	def updateCaseUrl = "${urlRoot}/api/v2/update_case/"+it3.get("id")
                	def newRefs = refs + ", "+issue.key
                	log.debug "New refs: " + newRefs + " ;URL " + updateCaseUrl
                	// updateCase(updateCaseUrl, newRefs, authString)
                }
            }
        }
    }
    //*/

    // Test Runs
    /*
    def restruns = "${urlRoot}/api/v2/get_runs/${proj_id}"
    //log.debug "TestRail: Runs rest url: " + restruns
    def runs_result=getResult(restruns, authString)
    def get_tests = ""
    def run_id = ""    
    runs_result.eachWithIndex { it2, i2 ->
    	//log.debug "TestRail: Run ID: " + it2.get("id")
    	run_id = it2.get("id")
    	// Get tests for the run
    	get_tests =  "${urlRoot}/api/v2/get_tests/${run_id}"
    	//log.debug "TestRail: get_tests url: " + get_tests
    	def tests_results=getResult(get_tests, authString)
    	tests_results.eachWithIndex { it3, i3 ->
    		def refs = it3.get("refs")
    		if (refs.toString()!="null") {
    			log.debug "TestRail: Test ID: " + it3.get("id") + " Refs: " + refs
    		}
    	}
    }
    */
}

/*
// testing runs & tests
def restruns = "${urlRoot}/api/v2/get_results/43839"
log.debug "Run url: " + restruns
def runs_result=getResult(restruns, authString)
log.debug "Run result: " + runs_result
*/
// Test ID: 87765 Refs: CFD-1004, CFD-2734, CFD-2643
/*
restruns = "${urlRoot}/api/v2/get_test/87765"
log.debug "Test url: " + restruns
runs_result=getResult(restruns, authString)
log.debug "Test result: " + runs_result
log.debug "Test refs: " + runs_result.get("refs")
log.debug "Refs tokenize: " + runs_result.get("refs").tokenize(", ")

// Tests don't have API to change REF's
//*/

 //Case ID: 22507 Refs: CFD-1004, CFD-2734, CFD-2643
 // Testing cases through the issues
 /*
 def cases = "http://8.8.8.8/testrail/index.php?/api/v2/get_cases/3&suite_id=7"
 def cases_result=getResult(cases, authString)
 cases_result.eachWithIndex { it3, i3 ->
    def refs = it3.get("refs")
    if (refs.toString()!="null") {
                //log.debug "TestRail: Case ID: " + it3.get("id") + " Refs: " + refs
                log.debug "TestRail: Case Refs tokenize: " + refs.tokenize(", ")
                checkRefsInIssues = checkRefs(IssuesMap, refs.tokenize(", "))
                log.debug "TestRail: Case Ref presence in Issues Map: " + checkRefsInIssues
    }
}
*/