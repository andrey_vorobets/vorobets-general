﻿-- get issues for MK & MKJ projects
select p2.pkey || '-' || p1.issuenum as issuekey, p1.id, p1.updated, p2.id from jiraissue p1,  project p2 where p1.project=p2.id and p2.pkey in ('MK', 'MKJ') 
and extract(month from p1.updated) in ('4', '5') and extract(year from p1.updated)='2016'
order by p1.project, p1.issuenum

-- Dor - 11002 , issue for example - 26063 (MKJ-369)
-- get value for DoR field
select p1.issue, p2.customvalue, p4.pkey || '-' || p3.issuenum as issuekey from customfieldvalue p1, customfieldoption p2, jiraissue p3, project p4 where 
p1.customfield='11002' and p2.customfield=p1.customfield and p3.id=p1.issue and p4.id=p3.project and p1.issue='26063' limit 20

-- Definition comment - 11003
select id from jiraissue where issuenum='732' and project='10300'
-- 21716
select textvalue from customfieldvalue where issue='21716' and customfield='11003'

-- DoD - 11001
select id from jiraissue where issuenum='889' and project='10300'
-- 24153
explain select p1.issue, p2.customvalue, p4.pkey || '-' || p3.issuenum as issuekey 
from customfieldvalue p1, customfieldoption p2, jiraissue p3, project p4 where 
p1.customfield='11001' and p2.customfield=p1.customfield and p3.id=p1.issue and p4.id=p3.project and p1.issue='24153' 
group by p1.issue, p2.customvalue, issuekey

-- Definition of done
-- 11004
select textvalue from customfieldvalue where issue='24153' and customfield='11004'

