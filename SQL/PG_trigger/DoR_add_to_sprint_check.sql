-- Trigger: dor_sprint_check_tr on customfieldvalue

-- DROP TRIGGER dor_sprint_check_tr ON customfieldvalue;

CREATE TRIGGER dor_sprint_check_tr
  BEFORE INSERT OR UPDATE
  ON customfieldvalue
  FOR EACH ROW
  EXECUTE PROCEDURE dor_sprint_check();
-- ALTER TABLE customfieldvalue DISABLE TRIGGER dor_sprint_check;

-- Function: dor_sprint_check()

-- DROP FUNCTION dor_sprint_check();

CREATE OR REPLACE FUNCTION dor_sprint_check()
  RETURNS trigger AS
$BODY$
	#plpy.warning(TD["new"])
	if TD["new"]['customfield']==10005:
		#log_msg = "Update or Insert Sprint field"
		#plpy.warning(log_msg)
		new_issue=TD["new"]['issue']
		sql="select pkey from project where id=(select project from jiraissue where id='%s')" % (str(new_issue))
		rv = plpy.execute(sql)
		pkey = rv[0]["pkey"]
		if pkey=="SAND":
			sql="select count(id) as countdor from customfieldvalue where customfield=(select id from customfield where cfname='DoR') and issue='%s'" % (str(new_issue))
			rv = plpy.execute(sql)
			try:
				count_dor=rv[0]["countdor"]
			except Exception, e:
				count_dor=0
			log_msg = "DoR Count: %s" % str(count_dor)
			plpy.warning(log_msg)
			sql="select textvalue from customfieldvalue where customfield=(select id from customfield where cfname='Definition Comment') and issue='%s'" % (str(new_issue))
			rv = plpy.execute(sql)
			try:
				dor_def=rv[0]["textvalue"]
			except Exception, e:
				dor_def=""
			log_msg = "DoR Definition: %s" % str(dor_def)
			plpy.warning(log_msg)

			if (count_dor>6 or len(dor_def)>5):
				return None				
			else:
				# Error will break transaction with sending Error to the Client
				#plpy.error("DoR or Definition must be filled")
				plpy.warning("DoR or Definition must be filled")
				return "SKIP";
		else:
			return None
	else:
		return None
$BODY$
  LANGUAGE plpythonu VOLATILE
  COST 100;
ALTER FUNCTION dor_sprint_check()
  OWNER TO postgres;