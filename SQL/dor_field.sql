-- In Jira 6.4.12 / Agile 6.7.12 can be used as DatabaseCustomField, in view too
-- In Jira 6.3.12 as DatabaseCustomField, select list, column dor
-- JNDI - PRDB
-- add to the Edit screen
-- Board - add to Card layout, Plan mode
select case 
when ((select stringvalue from customfieldvalue where customfield='10005' and issue=convert_to_integer({issueid})) is not null)
then 'DoR - in sprint'
when
(select 
(select count(id) from customfieldvalue where customfield=(select id from customfield where cfname='DoR')
 and issue=convert_to_integer({issueid}))
 +
(select case 
when ((select length(textvalue) from customfieldvalue where 
customfield=(select id from customfield where cfname='Definition Comment') and issue=convert_to_integer({issueid})) < 5) then 1
when ((select length(textvalue) from customfieldvalue where 
customfield=(select id from customfield where cfname='Definition Comment') and issue=convert_to_integer({issueid})) is null) then 1
else 10
end) as summ) < 9 then 'DoR is not filled, issue cannot be added to the sprint'
ELSE 'DoR is filled, issue is ready to the sprint'
end
as dor
-- function to cast issue id to number, used because cast from '' doesn't work
CREATE OR REPLACE FUNCTION convert_to_integer(v_input text)
RETURNS INTEGER AS $$
DECLARE v_int_value INTEGER DEFAULT NULL;
BEGIN
    BEGIN
        v_int_value := v_input::INTEGER;
    EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'Invalid integer value: "%".  Returning NULL.', v_input;
        RETURN 0;
    END;
RETURN v_int_value;
END;
$$ LANGUAGE plpgsql;

-- function for get current status, must be the same as for field
CREATE OR REPLACE FUNCTION dor_status(v_input text)
RETURNS TABLE (dor text) AS $func$
BEGIN
	RETURN QUERY
select case 
when ((select stringvalue from customfieldvalue where customfield='10005' and issue=convert_to_integer(v_input)) is not null)
then 'DoR - in sprint'
when
(select 
(select count(id) from customfieldvalue where customfield=(select id from customfield where cfname='DoR')
 and issue=convert_to_integer(v_input))
 +
(select case 
when ((select length(textvalue) from customfieldvalue where 
customfield=(select id from customfield where cfname='Definition Comment') and issue=convert_to_integer(v_input)) < 5) then 1
when ((select length(textvalue) from customfieldvalue where 
customfield=(select id from customfield where cfname='Definition Comment') and issue=convert_to_integer(v_input)) is null) then 1
else 10
end) as summ) < 9 then 'DoR is not filled, issue cannot be added to the sprint'
ELSE 'DoR is filled, issue is ready to the sprint'
end
as dor;
END
$func$ LANGUAGE plpgsql;