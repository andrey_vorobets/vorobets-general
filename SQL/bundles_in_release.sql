-- Bundles
-- Kepler Database custom field plugin
-- Type: Autocomplete
-- Multiple Values: Yes
-- JNDI: PRDB
-- Column: bundle_name
select p1."ID" || '. ' || p1."BUNDLE_NAME" || '  ' || p2.vname as bundle_name from "AO_102D86_BUNDLE" p1, projectversion p2 where p2.id=p1."VERSION_ID"