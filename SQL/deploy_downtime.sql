-- DROP TABLE deploy_downtime

-- examples
-- start downtime
-- insert into deploy_downtime (issueid, starttime) values ('issueid', now());
-- opened downtime
-- select count(id) from deploy_downtime where issueid='issueid' and endtime is null

CREATE TABLE deploy_downtime
(
id bigserial NOT NULL,
issueid numeric(18,0) NOT NULL,
starttime timestamp with time zone NOT NULL,
endtime timestamp with time zone,
CONSTRAINT pk_deploy_downtime PRIMARY KEY (id)
);

ALTER TABLE deploy_downtime OWNER TO jiratestdbuser;

-- DROP INDEX deploy_downtime_indx;

CREATE INDEX deploy_downtime_indx
  ON deploy_downtime
  USING btree
  (issueid);


-- Deploy downtime table view field
-- JNDI - PRDB
-- Thread caching: false
select to_char(starttime at time zone 'GMT', 'YYYY-MM-DD HH:MI') as start, to_char(endtime at time zone 'GMT', 'YYYY-MM-DD HH:MI') as end from deploy_downtime where issueid=convert_to_integer({issueid})
select to_char(starttime at time zone 'GMT', 'YYYY-MM-DD HH:MI') as start, to_char(endtime at time zone 'GMT', 'YYYY-MM-DD HH:MI') as end, to_char(endtime-starttime, 'HH24:MI:SS') as duration from deploy_downtime where issueid=convert_to_integer({issueid}) order by start desc